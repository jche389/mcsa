App({
    onLaunch: function() {
        console.log(this);
        // check update
        const updateManager = wx.getUpdateManager()
        updateManager.onCheckForUpdate(function (res) {
          // 请求完新版本信息的回调
          console.log(res.hasUpdate)
        })
        updateManager.onUpdateReady(function () {
          wx.showModal({
            title: '更新提示',
            content: '新版本已经准备好，是否重启应用？',
            success: function (res) {
              if (res.confirm) {
                // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                updateManager.applyUpdate()
              }
            }
          })
        })
        updateManager.onUpdateFailed(function () {
          // 新的版本下载失败
          wx.showModal({
            title: '更新提示',
            content: '新版本下载失败',
            showCancel: false
          })
        })
    },
    onShow: function() {
        console.log(getCurrentPages());
    },
    onHide: function() {
        console.log(getCurrentPages());
    },
    onError: function(e) {
        console.log(e);
    },
    util: require("we7/resource/js/util.js"),
    tabBar: {
        color: "#FFFFFF",
        selectedColor: "#FFFFFF",
        borderStyle: "#FFFFFF",
        backgroundColor: "#fff",
        list: [ {
            pagePath: "/mii_ang/pages/home/home",
            iconPath: "/images/icon/icons-home-us.png",
            selectedIconPath: "/images/icon/icons-home-us.png",
            text: "home"
        }, {
            pagePath: "/mii_ang/pages/service/service",
            iconPath: "/images/icon/icons-events-us.png",
            selectedIconPath: "/images/icon/icons-events-s.png",
            text: "events"
        }, {
            pagePath: "/mii_ang/pages/tech/index",
            iconPath: "/images/icon/tech.png",
            selectedIconPath: "/images/icon/techselect.png",
            text: "coupons"
        }, {
            pagePath: "/mii_ang/pages/personal/index",
            iconPath: "/images/icon/personal.png",
            selectedIconPath: "/images/icon/personalselect.png",
            text: "profile"
        } ]
    },
    globalData: {
        userInfo: null,
        webInfo: ""
    },
    siteInfo: require("siteinfo.js")
});