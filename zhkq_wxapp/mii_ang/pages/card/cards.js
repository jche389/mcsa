var app = getApp();

Page({
    data: {
      text: "微思出品",
        menlist: [ {
            type: "mycard",
            name: "已领取",
            url: "entry/wxapp/getmycardlist"
        }, {
            type: "cardall",
            name: "可领取",
            url: "entry/wxapp/getallcardlist"
        } ],
        active: {
            type: "mycard",
            url: "entry/wxapp/getmycardlist",
            openname: "使用"
        },
        cardList: {},
        nocard: "",
        cardinfo: {},
        upcard: {},
        openid: ""
    },
    onLoad: function(a) {
        var t = this;
        setTimeout(function() {
            wx.setNavigationBarColor({
                frontColor: "#000000",
                backgroundColor: "#ffffff",
                animation: {
                    duration: 400,
                    timingFunc: "easeIn"
                }
            }), wx.setNavigationBarTitle({
                title: "我的卡券"
            });
        }, 100), t.getcardlist(t.data.active.type), wx.login({
            success: function(a) {
                app.util.request({
                    url: "entry/wxapp/useropenid",
                    data: {
                        code: a.code
                    },
                    cachetime: "30",
                    success: function(a) {
                        t.setData({
                            openid: a.data
                        }), t.getcardlist(a.data);
                    }
                });
            }
        });
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    changemen: function(a) {
        this.setData({
            cardList: ""
        }), this.setData({
            active: {
                type: a.target.dataset.type,
                url: a.target.dataset.url,
                openname: "mycard" == a.target.dataset.type ? "使用" : "领取"
            }
        }), this.getcardlist(this.data.openid);
    },
    getcardlist: function(a) {
        wx.showNavigationBarLoading();
        var t = this;
        app.util.request({
            url: t.data.active.url,
            data: {
                openid: a
            },
            cachetime: "10",
            success: function(a) {
                t.setData({
                    cardList: a.data,
                    nocard: "" == a.data ? "1" : ""
                }), console.log(a);
            }
        });
    },
    addcard: function(d) {
        var r = this;
        "function" == typeof wx.addCard ? (wx.showLoading({
            title: "加载中",
            mask: !0
        }), 
        app.util.request({
            url: "entry/wxapp/getcard",
            data: {
                openid: r.data.openid,
                card_id: d.target.dataset.carid
            },
            cachetime: "30",
            success: function(e) {
                r.setData({
                    cardinfo: e
                }),
                wx.addCard({
                    cardList:
                    [{
                        cardId: e.data.cardArray.cardId,
                        cardExt: '{"code": "", "openid":' + e.data.cardArray.openid + ' ,"timestamp":' + e.data.cardArray.timestamp + ', "signature":' + e.data.cardArray.signature + "}"
                    }],
                    success: function(a) {
                        var t = a.cardList;
                        app.util.request({
                            url: "entry/wxapp/insCard",
                            data: {
                                openid: e.data.cardArray.openid,
                                cardid: e.data.cardArray.cardId,
                                code: t
                            },
                            cachetime: "30",
                            success: function(a) {
                                r.setData({
                                    incard: "OK"
                                }), 
                                app.util.request({
                                    url: "entry/wxapp/upcard",
                                    data: {
                                        card_id: e.data.cardArray.cardId,
                                        card_type: d.target.dataset.carty
                                    },
                                    cachetime: "30",
                                    success: function(a) {
                                        r.setData({
                                            upcard: "OK"
                                        });
                                    }
                                });
                            }
                        });
                    },
                    fail: function(a) {
                        wx.hideLoading(), 
                        console.log(a);
                    }
                });
            }
        })) : wx.showLoading({
            title: "失败",
            mask: !0
        });
    },
    opencard: function(a) {
        wx.openCard({
            cardList: [ {
                cardId: a.target.dataset.carid,
                code: ""
            } ],
            success: function(a) {}
        }), console.log(a);
    }
});