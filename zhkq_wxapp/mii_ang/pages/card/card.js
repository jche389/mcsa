var _Page;

function _defineProperty(t, a, e) {
    return a in t ? Object.defineProperty(t, a, {
        value: e,
        enumerable: !0,
        configurable: !0,
        writable: !0
    }) : t[a] = e, t;
}

var app = getApp();

Page((_defineProperty(_Page = {
    data: {
      text: "微思出品",
        menlist: [ {
            type: "mycard",
            name: "已领取",
            url: "entry/wxapp/getmycardlist"
        }, {
            type: "cardall",
            name: "可领取",
            url: "entry/wxapp/getcardlist"
        } ],
        active: {
            type: "mycard",
            url: "entry/wxapp/getmycardlist"
        },
        nocard: !1,
        toast: {
            isHide: !1,
            content: ""
        },
        input: {
            isHide: !1,
            uname: "",
            uphone: ""
        },
        uname: "",
        uphone: ""
    },
    onLoad: function(t) {
        this.GetuserInfo(), this.Getcardlist();
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    changemen: function(t) {
        this.setData({
            cardList: ""
        }), this.setData({
            active: {
                type: t.target.dataset.type,
                url: t.target.dataset.url
            }
        }), this.Getcardlist();
    },
    Getcardlist: function() {
        var a = this;
        app.util.request({
            url: a.data.active.url,
            data: {},
            cachetime: "10",
            success: function(t) {
                "1" == t.data.erron ? a.setData({
                    cardList: t.data.cardlist,
                    nocard: "1" == t.data.nocard
                }) : a.setData({
                    nocard: "1" == t.data.nocard
                }), setTimeout(function() {
                    wx.setNavigationBarColor({
                        frontColor: "#000000",
                        backgroundColor: "#ffffff",
                        animation: {
                            duration: 400,
                            timingFunc: "easeIn"
                        }
                    }), wx.setNavigationBarTitle({
                        title: t.data.title
                    });
                }, 100);
            }
        });
    },
    GetuserInfo: function() {
        var a = this;
        app.util.getUserInfo(function(t) {
            a.setData({
                sessionid: t.sessionid
            });
        });
    },
    wx_addcard: function(t) {
        var e = this, i = t.currentTarget.dataset.cid, n = t.currentTarget.dataset.oid, r = t.currentTarget.dataset.tid, d = t.currentTarget.dataset.iid;
        app.util.request({
            url: "entry/wxapp/addcard",
            data: {
                card_id: i
            },
            cachetime: "0",
            success: function(t) {
                console.log(t), 
                wx.addCard({
                    cardList: [ {
                        cardId: t.data.cardArray.cardId,
                        cardExt: '{"code": "", "openid":"' + t.data.cardArray.openid + '", "timestamp":"' + t.data.cardArray.timestamp + '","nonce_str":"' + t.data.cardArray.nonceStr + '", "signature":"' + t.data.cardArray.signature + '"}'
                    } ],
                    success: function(t) {
                        var a = t.cardList;
                        app.util.request({
                            url: "entry/wxapp/insCard",
                            data: {
                                card_id: i,
                                code: a,
                                card_on: n,
                                card_type: r,
                                iid: d
                            },
                            cachetime: "30",
                            success: function(t) {
                                e.show(t.data.msg);
                            }
                        });
                    },
                    fail: function(t) {
                        wx.hideLoading(), console.log(t);
                    }
                });
            }
        });
    },
    mii_addcard: function(t) {
        var a = this, e = t.currentTarget.dataset.cid, i = t.currentTarget.dataset.oid, n = t.currentTarget.dataset.tid, r = t.currentTarget.dataset.pid, d = t.currentTarget.dataset.iid, s = "cardList[" + r + "].card_on";
        app.util.request({
            url: "entry/wxapp/addcardAndmii",
            data: {
                iid: d,
                card_id: e,
                card_on: i,
                card_type: n
            },
            cachetime: "0",
            success: function(t) {
                "1" == t.data.status ? (a.setData(_defineProperty({}, s, t.data.card_on)), a.show(t.data.msg)) : "2" == t.data.status ? (a.show(t.data.msg), 
                setTimeout(function() {
                    a.bind();
                }, 1e3)) : a.show(t.data.msg);
            }
        });
    },
    show: function(t) {
        var a = this;
        this.setData({
            "toast.isHide": !0,
            "toast.content": t
        }), setTimeout(function() {
            a.setData({
                "toast.isHide": !1
            });
        }, 2e3);
    },
    save: function(t) {
        var a = this, e = a.data.uid, i = a.data.input.uname, n = a.data.input.uphone;
        return "" == e ? (a.show("请输入姓名"), !1) : "" == i ? (a.show("请输入姓名"), !1) : "" == n ? (a.show("请输入联系电话"), 
        !1) : /^13[0-9]{9}$|14[0-9]{9}$|15[0-9]{9}$|18[0-9]{9}$|17[0-9]{9}$/.test(n) ? void app.util.request({
            url: "entry/wxapp/memberbind",
            data: {
                uname: i,
                uphone: n,
                uid: e
            },
            cache: "0",
            success: function(t) {
                "1" == t.data.status && a.setData({
                    "input.isHide": !1
                }), a.show(t.data.msg), console.log(t);
            }
        }) : (a.show("联系电话错误"), !1);
    }
}, "show", function(t) {
    var a = this;
    this.setData({
        "toast.isHide": !0,
        "toast.content": t
    }), setTimeout(function() {
        a.setData({
            "toast.isHide": !1
        });
    }, 2e3);
}), _defineProperty(_Page, "bind", function() {
    this.setData({
        "input.isHide": !0
    });
}), _defineProperty(_Page, "close", function(t) {
    this.setData({
        "input.isHide": !1
    });
}), _defineProperty(_Page, "uname", function(t) {
    this.setData({
        "input.uname": t.detail.value
    });
}), _defineProperty(_Page, "uphone", function(t) {
    this.show("请点击左侧小图标获取");
}), _defineProperty(_Page, "goindex", function() {
    wx.navigateTo({
        url: "/mii_ang/pages/index/index"
    });
}), _defineProperty(_Page, "getPhoneNumber", function(a) {
    var e = this;
    wx.login({
        success: function(t) {
            console.log(a), "getPhoneNumber:ok" == a.detail.errMsg ? app.util.request({
                url: "entry/wxapp/decode",
                data: {
                    iv: a.detail.iv,
                    data: a.detail.encryptedData,
                    code: t.code
                },
                cachetime: "0",
                success: function(t) {
                    console.log(t), "1" == t.data.status ? e.setData({
                        "input.uphone": t.data.phoneNumber
                    }) : e.show(t.data.msg);
                }
            }) : e.show("获取失败,请允许系统获取您的手机号码");
        }
    });
}), _Page));