// mii_ang/pages/events/event-detail.js
const app = getApp();

Page({
  /**
   * 页面的初始数据
   */
  data: {
    eventList: [],
    swiperCurrent: 0,
    recentSwiperCurrent: 0,
    eventPageSize: 4,
    tabBarData: {
      thisurl: "",
      borderStyle: "#d9d9d9",
      backgroundColor: "#fff",
      list: []
    },
    url: ""
  },

  /**
   * 获取底部菜单栏
   */
  getTabBarData: function() {
    var a = this;
    app.util.request({
      url: "entry/wxapp/TabBarData",
      data: {},
      cachetime: "30",
      success: function(t) {
        console.log("tabBar", t.data);
        a.setData({
          "tabBarData.list": t.data.tabBarData
        });
      }
    });
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function(options) {
    console.log(options);
    var a = this;
    console.log("eventconsole", a.data);
    wx.setNavigationBarTitle({
      title: "活 动 详 情"
    });
    wx.setNavigationBarColor({
      frontColor: "#ffffff",
      backgroundColor: "#101B39"
    });
    // 加载底部菜单
    a.getTabBarData();
    // 获取活动参数
    a.setData({
      id: options.id,
      name: options.name,
      address: options.address,
      start: options.start,
      end: options.end,
      bm_start: options.bm_start,
      bm_end: options.bm_end,
      imageUrl: options.imageUrl,
      cost: options.cost,
      bsb: options.bsb ? options.bsb : 0,
      acc: options.acc ? options.acc : 0,
      acc_name: options.acc_name ? options.acc_name : "",
      content: a.removeTags(options.content)
    });
  },

  /**
   * 去除html标签
   */
  removeTags: function(str) {
    str = str.replace("<p>", "");
    str = str.replace("</p>", "");
    return str;
  },

  /**
   * 活动报名
   */
  applyEvent: function(e) {
    var a = this;
    var uid = wx.getStorageSync("uid_bm"); // 用户id
    console.log(uid);
    console.log(a.data.id);
    // 判断是否重复报名
    app.util.request({
      url: "entry/wxapp/checkenroll/zh_hdbm",
      cachetime: "0",
      data: {
        id: a.data.id,
        user_id: uid
      },
      success: function(res) {
        console.log(res);
        if (res.data == 1) {
          wx.setNavigationBarTitle({
            title: "活动详情"
          });
          wx.setNavigationBarColor({
            frontColor: "#ffffff",
            backgroundColor: "#101B39"
          });
          wx.navigateTo({
            url: e.currentTarget.dataset.url
          });
        } else {
          wx: wx.showToast({
            title: "请勿重复报名",
            icon: "",
            image: "",
            duration: 3000,
            mask: true,
            success: function(res) {},
            fail: function(res) {},
            complete: function(res) {}
          });
        }
      }
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function() {},

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function() {},

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function() {},

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function() {},

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function() {},

  /**
   * Called when page reach bottom
   */
  onReachBottom: function() {},

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function() {}
});
