// mii_ang/pages/events/event-apply.js
const app = getApp();

Page({
  /**
   * Page initial data
   */
  data: {},

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function(options) {
    console.log(options);
    var a = this;
    wx.setNavigationBarTitle({
      title: "线 上 报 名"
    });
    wx.setNavigationBarColor({
      frontColor: "#ffffff",
      backgroundColor: "#101B39"
    });
    // 获取活动参数
    a.setData({
      id: options.id,
      name: options.name,
      start: options.start.substring(0, 16),
      end: options.end.substring(10, 16),
      cost: options.cost,
      bsb: options.bsb,
      acc: options.acc,
      acc_name: options.acc_name
    });
  },

  /**
   * 提交表单
   */
  apply: function(e) {
    var a = this;
    console.log(a.data.id);
    // 用户信息
    var surname = e.detail.value.surname; // 姓氏
    var firstname = e.detail.value.firstname; // 名字
    var phone = e.detail.value.phone.trim(); // 电话
    var email = e.detail.value.email.trim(); // email
    var uid = wx.getStorageSync("uid_bm"); // 用户id
    var avatar = wx.getStorageSync("img"); // 用户头像
    console.log(e.detail.value);

    // 表单验证
    var phoneReg = new RegExp(/^\d{10}$/);
    var emailReg = new RegExp(
      "^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(.[a-zA-Z0-9-]+)*.[a-zA-Z0-9]{2,6}$"
    );
    console.log(phone);
    console.log(phoneReg.test(phone));
    console.log(emailReg.test(email));
    // 非空校验
    if (
      surname.trim().length == 0 ||
      firstname.trim().length == 0 ||
      phone.trim().length == 0
    ) {
      wx: wx.showToast({
        title: "请输入完整信息",
        icon: "",
        image: "",
        duration: 3000,
        mask: true,
        success: function(res) {},
        fail: function(res) {},
        complete: function(res) {}
      });
    } else if (!phoneReg.test(phone)) {
      wx: wx.showToast({
        title: "电话格式不正确",
        icon: "",
        image: "",
        duration: 3000,
        mask: true,
        success: function(res) {},
        fail: function(res) {},
        complete: function(res) {}
      });
    } else if (email.trim() != "" && !emailReg.test(email)) {
      wx: wx.showToast({
        title: "邮箱格式错误",
        icon: "",
        image: "",
        duration: 3000,
        mask: true,
        success: function(res) {},
        fail: function(res) {},
        complete: function(res) {}
      });
    } else {
      // 报名
      app.util.request({
        url: "entry/wxapp/saveenroll/zh_hdbm",
        cachetime: "0",
        data: {
          id: a.data.id,
          user_id: uid,
          name: surname + " " + firstname,
          tel: phone,
          logo: avatar,
          email: email
        },
        success: function(res) {
          console.log("报名成功", res);
          console.log(a.data);
          wx.reLaunch({
            // url: 'apply-result?res=' + res
            url:
              "apply-result?id=" +
              res.data +
              "&start=" +
              a.data.start +
              "&end=" +
              a.data.end +
              "&cost=" +
              a.data.cost +
              "&name=" +
              a.data.name +
              "&bsb=" +
              a.data.bsb +
              "&acc=" +
              a.data.acc +
              "&acc_name=" +
              a.data.acc_name +
              "&user_name=" +
              surname +
              " " +
              firstname +
              "&mobile=" +
              phone
          });
        }
      });
    }
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function() {},

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function() {},

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function() {},

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function() {},

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function() {},

  /**
   * Called when page reach bottom
   */
  onReachBottom: function() {},

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function() {}
});
