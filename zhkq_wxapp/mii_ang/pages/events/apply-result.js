// mii_ang/pages/events/apply-result.js
Page({
  /**
   * Page initial data
   */
  data: {},

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function(options) {
    console.log(options);

    var a = this;
    a.setData({
      id: options.id,
      name: options.name,
      start: options.start,
      end: options.end,
      cost: options.cost,
      bsb: options.bsb,
      acc: options.acc,
      acc_name: options.acc_name,
      user_name: options.user_name,
      mobile: options.mobile
    });
    wx.setNavigationBarTitle({
      title: "报 名 完 成"
    });
    wx.setNavigationBarColor({
      frontColor: "#ffffff",
      backgroundColor: "#101B39"
    });
  },

  /**
   * 完成
   */
  complete: function() {
    wx.redirectTo({
      url: "events"
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function() {},

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function() {},

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function() {},

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function() {},

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function() {},

  /**
   * Called when page reach bottom
   */
  onReachBottom: function() {},

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function() {}
});
