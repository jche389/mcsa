const app = getApp();

// mii_ang/pages/events/events.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    eventList: [],
    swiperCurrent: 0,
    recentSwiperCurrent: 0,
    eventPageSize: 4,
    tabBarData: {
      thisurl: "mii_ang/pages/events/events",
      borderStyle: "#d9d9d9",
      backgroundColor: "#fff",
      list: []
    },
    url: ""
  },

  /**
   * swiper change handler
   */
  eventSwiperChange: function(e) {
    this.setData({
      swiperCurrent: e.detail.current
    });
  },

  /**
   * swiper change handler
   */
  recentSwiperChange: function(e) {
    this.setData({
      swiperCurrent: e.detail.current
    });
  },

  /**
   * 下一个滚动图片
   */
  nextSwiper: function(e) {
    var a = this;
    var next =
      a.data.swiperCurrent == a.data.eventList.length - 1
        ? 0
        : a.data.swiperCurrent + 1;
    this.setData({
      swiperCurrent: next
    });
  },

  /**
   * 上一个滚动图片
   */
  prevSwiper: function() {
    var a = this;
    var next =
      a.data.swiperCurrent == 0
        ? a.data.eventList.length - 1
        : a.data.swiperCurrent - 1;
    this.setData({
      swiperCurrent: next
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var a = this;
    wx.setNavigationBarTitle({
      title: "所 有 活 动"
    });
    wx.setNavigationBarColor({
      frontColor: "#ffffff",
      backgroundColor: "#101B39"
    });
    // 获取图片URL
    a.getUrl();
    // 加载底部菜单
    a.getTabBarData();
    // 获取活动
    a.getEventList();
  },

  /**itemiteitemtitemsdfssdfsdfssditem
   * 获取活动
   */
  getEventList: function() {
    var a = this;
    //活动的接口
    app.util.request({
      url: "entry/wxapp/activity/zh_hdbm",
      headers: {
        "Content-Type": "application/json"
      },
      cachetime: "0",
      success: function(res) {
        console.log(res);
        // a.eventPaging(res);
        a.setData({
          eventList: res.data
        });
      }
    });
  },

  /**
   * 活动分页
   */
  // eventPaging: function (t) {
  //   var a = this;
  //   var eventList = t.data;
  //   if (eventList.length <= a.data.eventPageSize) {
  //     a.setData({
  //       eventList: [eventList]
  //     });
  //     console.log(a.data.eventList);
  //   } else {
  //     var index = 0;
  //     var page = 0;
  //     var newEventList = [];
  //     var subList = [];
  //     var subIndex = 0;
  //     for (index; index < eventList.length; index++) {
  //       if (subIndex == a.data.eventPageSize) {
  //         subList = [];
  //         subIndex = 0;
  //         subList[subIndex++] = eventList[index];
  //         newEventList[++page] = subList;
  //       } else {
  //         subList[subIndex++] = eventList[index];
  //         newEventList[page] = subList;
  //       }
  //     }
  //     a.setData({
  //       eventList: newEventList,
  //       nocard: "1" == t.data.nocard
  //     });
  //     console.log(a.data.eventList);
  //   }
  // },

  /**
   * 活动详情
   */
  viewDetail: function(e) {
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    });
  },

  /**
   * 获取服务器路径
   */
  getUrl: function() {
    var that = this;
    app.util.request({
      url: "entry/wxapp/attachurl/zh_hdbm",
      headers: {
        "Content-Type": "application/json"
      },
      cachetime: "0",
      success: function(res) {
        var url = res.data;
        wx.setStorageSync("url", res.data);
        that.setData({
          url: url
        });
        console.log(res);
      }
    });
  },

  /**
   * 获取底部菜单栏
   */
  getTabBarData: function() {
    var a = this;
    app.util.request({
      url: "entry/wxapp/TabBarData",
      data: {},
      cachetime: "30",
      success: function(t) {
        console.log("tabBar", t.data);
        a.setData({
          "tabBarData.list": t.data.tabBarData
        });
      }
    });
  }
});
