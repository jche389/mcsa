// mii_ang/pages/coupon/coupon.js
const app = getApp();

Page({
  /**
   * 页面的初始数据
   */
  data: {
    activityList: [],
    swiperCurrent: 0,
    swiperCardCurrent: 0,
    recentSwiperCurrent: 0,
    cardPageSize: 3,
    tabBarData: {
      thisurl: "mii_ang/pages/coupons/coupons",
      borderStyle: "#d9d9d9",
      backgroundColor: "#fff",
      list: []
    },
    url: "",
    active: {
      type: "cardall",
      url: "entry/wxapp/getcardlistwithlogo"
    }
  },
  seeMoreCoupons: function() {
    wx.navigateTo({
      url: "../coupons/coupons"
    });
  },
  /**
   * swiper change handler
   */
  cardSwiperChange: function(e) {
    this.setData({
      swiperCardCurrent: e.detail.current
    });
  },

  /**
   * swiper change handler
   */
  swiperChange: function(e) {
    this.setData({
      swiperCurrent: e.detail.current
    });
  },

  /**
   * 下一个滚动图片
   */
  nextSwiper: function(e) {
    var a = this;
    var next =
      a.data.swiperCurrent == a.data.activityList.length - 1
        ? 0
        : a.data.swiperCurrent + 1;
    this.setData({
      swiperCurrent: next
    });
  },

  /**
   * 上一个滚动图片
   */
  prevSwiper: function() {
    var a = this;
    var next =
      a.data.swiperCurrent == 0
        ? a.data.activityList.length - 1
        : a.data.swiperCurrent - 1;
    this.setData({
      swiperCurrent: next
    });
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function(options) {
    var that = this;
    wx.setNavigationBarTitle({
      title: "所 有 折 扣"
    });
    wx.setNavigationBarColor({
      frontColor: "#ffffff",
      backgroundColor: "#922527"
    });
    // 获取图片URL
    that.getUrl();
    // 加载底部菜单
    that.getTabBarData();
    // 获取用户信息
    that.getUserInfo();
    // 获取卡券
    if (options.type != undefined) {
      var t = {
        currentTarget: { dataset: { type: options.type } }
      };
      that.getCardListByType(t);
    } else {
      that.getCardList();
    }
    // 获取商家信息
    that.getCompanyList();
  },

  /**
   * 获取服务器路径
   */
  getUrl: function() {
    var that = this;
    app.util.request({
      url: "entry/wxapp/attachurl/zh_hdbm",
      headers: {
        "Content-Type": "application/json"
      },
      cachetime: "0",
      success: function(res) {
        var url = res.data;
        wx.setStorageSync("url", res.data);
        that.setData({
          url: url
        });
        console.log(res);
      }
    });
  },

  /**
   * 获取用户信息
   */
  getUserInfo: function() {
    var a = this;
    console.log("Getting user information");
    app.util.getUserInfo(function(t) {
      a.setData({
        sessionid: t.sessionid
      });
    });
  },

  /**
   * 获取商家信息
   */
  getCompanyList: function() {
    var that = this;
    app.util.request({
      url: "entry/wxapp/getcompanylist",
      data: {},
      success: function(t) {
        console.log(t);
        that.setData({
          companyList: t.data
        });
        console.log(that.data.companyList);
      }
    });
  },

  /**
   * 获取卡券信息
   */
  getCardList: function() {
    var a = this;
    console.log(a.data.active.url);
    app.util.request({
      url: a.data.active.url,
      data: {},
      cachetime: "10",
      success: function(t) {
        console.log(t);
        // a.setData({
        //   allCards: t.data.cardlist
        // })
        a.formateDate(t.data.cardlist);
        t.data.cardlist.length > 0
          ? a.setData({ nocard: false, cardList: t.data.cardlist })
          : a.setData({ nocard: "1" == t.data.nocard, cardList: [] });
      }
    });
  },

  /**
   * 领取卡券
   */
  wx_addcard: function(t) {
    // console.log('Weboost-JL: t', t);
    var that = this;
    // console.log('Weboost-JL: that', that);
    var i = t.currentTarget.dataset.cid,
      n = t.currentTarget.dataset.oid,
      r = t.currentTarget.dataset.tid,
      d = t.currentTarget.dataset.iid;
    app.util.request({
      url: "entry/wxapp/myaddcard",
      data: {
        card_id: i,
        cid: t.currentTarget.dataset.cid
      },
      cachetime: "30",
      success: function(t) {
        console.log(t.data);
        wx.addCard({
          cardList: [
            {
              cardId: t.data.cardArray.cardId,
              cardExt:
                '{"code": "", "openid":"' +
                t.data.cardArray.openid +
                '", "timestamp":"' +
                t.data.cardArray.timestamp +
                '","nonce_str":"' +
                t.data.cardArray.nonceStr +
                '", "signature":"' +
                t.data.cardArray.signature +
                '"}'
            }
          ],
          success: function(t) {
            console.log(t);
            var a = t.cardList;
            app.util.request({
              url: "entry/wxapp/decryptcode",
              data: {
                code: t.cardList[0].code
              },
              cachetime: "30",
              success: function(t) {
                console.log(t.data);
                var code = JSON.parse(t.data.content).code;
                app.util.request({
                  url: "entry/wxapp/insCard",
                  data: {
                    card_id: i,
                    code: code,
                    card_on: n,
                    card_type: r,
                    iid: d
                  },
                  cachetime: "30",
                  success: function(t) {
                    that.show(t.data.msg);
                    that.getCardList();
                  }
                });
                // wx.openCard({
                //   cardList: [{
                //     cardId: i,
                //     code: code
                //   }],
                //   success: function (t) {
                //     console.log("开卡成功");
                //   }
                // })
              }
            });
          },
          fail: function(t) {
            wx.hideLoading(), console.log(t);
          }
        });
      }
    });
  },
  /**
   * 卡券分类
   */
  getCardListByType: function(t) {
    var that = this;
    var type = t.currentTarget.dataset.type;
    app.util.request({
      url: "entry/wxapp/getcardlistwithlogo",
      data: {
        type: type
      },
      cachetime: "30",
      success: function(t) {
        console.log(t);
        that.formateDate(t.data.cardlist);
        t.data.cardlist.length > 0
          ? that.setData({ nocard: false, cardList: t.data.cardlist })
          : that.setData({ nocard: "1" == t.data.nocard, cardList: [] });
      }
    });
  },

  /**
   * 日期格式转换
   */
  formateDate: function(cardList) {
    for (let card of cardList) {
      if (card.date.indexOf("DATE_TYPE_FIX_TERM") > -1) {
        var dateInfo = JSON.parse(card.date);
        card.date = "自领取之日";
        card.date +=
          dateInfo.fixed_begin_term == 0
            ? "当日"
            : dateInfo.fixed_begin_term + "天后 ";
        card.date += dateInfo.fixed_term + "天内有效";
      }
    }
  },

  /**
   * 卡券分页
   */
  // cardPaging: function (t) {
  //   var a = this;
  //   var cardList = t.data.cardlist;
  //   console.log(cardList);
  //   if (cardList.length <= a.data.cardPageSize) {
  //     a.setData({
  //       cardList: [cardList],
  //       nocard: "1" == t.data.nocard
  //     })
  //   } else {
  //     var index = 0;
  //     var page = 0;
  //     var newCardList = [];
  //     var subList = [];
  //     var subIndex = 0;
  //     for (index; index < cardList.length; index++) {
  //       if (cardList[index].card_type == "MEMBER_CARD") {
  //         continue;
  //       }
  //       if (subIndex == a.data.cardPageSize) {
  //         subList = [];
  //         subIndex = 0;
  //         subList[subIndex++] = cardList[index];
  //         newCardList[++page] = subList;
  //       } else {
  //         subList[subIndex++] = cardList[index];
  //         newCardList[page] = subList;
  //       }
  //     }
  //     a.setData({
  //       cardList: newCardList,
  //       nocard: "1" == t.data.nocard
  //     });
  //     console.log(a.data.cardList);
  //   }
  // },

  /**
   * 显示提示信息
   */
  show: function(t) {
    var a = this;
    this.setData({
      "toast.isHide": !0,
      "toast.content": t
    }),
      setTimeout(function() {
        a.setData({
          "toast.isHide": !1
        });
      }, 2e3);
  },

  /**
   * 查看商家页面
   */
  viewCompany: function(e) {
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    });
  },

  /**
   * 获取底部菜单栏
   */
  getTabBarData: function() {
    var a = this;
    app.util.request({
      url: "entry/wxapp/TabBarData",
      data: {},
      cachetime: "30",
      success: function(t) {
        console.log("tabBar", t.data);
        a.setData({
          "tabBarData.list": t.data.tabBarData
        });
      }
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function() {},

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function() {},

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function() {},

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function() {},

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function() {},

  /**
   * Called when page reach bottom
   */
  onReachBottom: function() {},

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function() {}
});
