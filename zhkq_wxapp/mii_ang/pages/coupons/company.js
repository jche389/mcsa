// mii_ang/pages/coupons/company.js
const app = getApp();

Page({
  /**
   * Page initial data
   */
  data: {
    cardPageSize: 1,
    flag: false
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function(options) {
    console.log(options);
    var a = this;
    // 加载标题栏
    wx.setNavigationBarTitle({
      title: "商 家 介 绍"
    });
    wx.setNavigationBarColor({
      frontColor: "#ffffff",
      backgroundColor: "#922527"
    });
    // 获取图片URL
    a.getUrl();
    // 加载底部菜单
    a.getTabBarData();
    // 获取商家信息
    a.setData({
      companyid: options.companyid,
      logo: options.logo,
      companyname: options.companyname,
      address: options.address,
      phone: options.phone,
      companyinfo: options.companyinfo,
      discountinfo: options.discountinfo
    });
    // 获取卡券
    a.getCardList();
    //
  },

  /**
   * 获取底部菜单栏
   */
  getTabBarData: function() {
    var a = this;
    app.util.request({
      url: "entry/wxapp/TabBarData",
      data: {},
      cachetime: "30",
      success: function(t) {
        console.log("tabBar", t.data);
        a.setData({
          "tabBarData.list": t.data.tabBarData
        });
      }
    });
  },

  /**
   * 获取服务器路径
   */
  getUrl: function() {
    var that = this;
    app.util.request({
      url: "entry/wxapp/attachurl/zh_hdbm",
      headers: {
        "Content-Type": "application/json"
      },
      cachetime: "0",
      success: function(res) {
        var url = res.data;
        wx.setStorageSync("url", res.data);
        that.setData({
          url: url
        });
        console.log(res);
      }
    });
  },

  /**
   * 获取卡券信息
   */
  getCardList: function() {
    var a = this;
    console.log("Weboost-JL: a", a);
    app.util.request({
      url: "entry/wxapp/getcardlistbycompany",
      data: {
        companyid: a.data.companyid
      },
      cachetime: "0",
      success: function(t) {
        console.log(t.data);
        a.setData({
          cardList: t.data
        });
        t.data.length > 0
          ? a.setData({
              cardList: t.data
            })
          : a.setData({
              nocard: "1" == t.data.nocard
            });
      }
    });
  },

  /**
   * 防止button多次点击提交
   */

  showLoading: function(message) {
    if (wx.showLoading) {
      // 基础库 1.1.0 微信6.5.6版本开始支持，低版本需做兼容处理
      wx.showLoading({ title: message, mask: true });
    } else {
      // 低版本采用Toast兼容处理并将时间设为20秒以免自动消失
      wx.showToast({
        title: message,
        icon: "loading",
        mask: true,
        duration: 20000
      });
    }
  },

  hideLoading: function() {
    if (wx.hideLoading) {
      // 基础库 1.1.0 微信6.5.6版本开始支持，低版本需做兼容处理
      wx.hideLoading();
    } else {
      wx.hideToast();
    }
  },
  //old add card
  wx_addcard1: function(t) {
    console.log(t.currentTarget.dataset);
    var that = this;
    var i = t.currentTarget.dataset.cid,
      n = t.currentTarget.dataset.oid,
      r = t.currentTarget.dataset.tid,
      d = t.currentTarget.dataset.iid;
    app.util.request({
      url: "entry/wxapp/myaddcard",
      data: {
        card_id: i
      },
      cachetime: "30",
      success: function(t) {
        console.log(t);
        wx.addCard({
          cardList: [
            {
              cardId: t.data.cardArray.cardId,
              cardExt:
                '{"code": "", "openid":"' +
                t.data.cardArray.openid +
                '", "timestamp":"' +
                t.data.cardArray.timestamp +
                '","nonce_str":"' +
                t.data.cardArray.nonceStr +
                '", "signature":"' +
                t.data.cardArray.signature +
                '"}'
            }
          ],
          success: function(t) {
            console.log(t);
            app.util.request({
              url: "entry/wxapp/decryptcode",
              data: {
                code: t.cardList[0].code
              },
              cachetime: "30",
              success: function(t) {
                console.log(JSON.parse(t.data.content));
                var code = JSON.parse(t.data.content).code;
                app.util.request({
                  url: "entry/wxapp/insCard",
                  data: {
                    card_id: i,
                    code: code,
                    card_on: n,
                    card_type: r,
                    iid: d
                  },
                  cachetime: "30",
                  success: function(t) {
                    that.show(t.data.msg);
                    that.getCardList();
                  }
                });
                //   wx.openCard({
                //     cardList: [{
                //       cardId: i,
                //       code: code
                //     }],
                //     success: function (t) {
                //       console.log("开卡成功");
                //     }
                //   })
              }
            });
          },
          fail: function(t) {
            wx.hideLoading(), console.log(t);
          }
        });
      }
    });
  },
  /**
   * 领取卡券
   */
  wx_addcard: function(t) {
    var that = this;
    var code = "";
    that.setData({
      flag: true
    });
    console.log(t);

    var i = t.currentTarget.dataset.cid,
      n = t.currentTarget.dataset.oid,
      r = t.currentTarget.dataset.tid,
      d = t.currentTarget.dataset.iid;
    var uid = wx.getStorageSync("uid_coupon");
    console.log(uid);
    app.util.request({
      url: "entry/wxapp/myaddcard",
      data: {
        card_id: i,
        uid: uid,
        cid: d,
        card_type: r
      },
      cachetime: "30",
      success: function(t) {
        console.log(t.data);
        wx.addCard({
          cardList: [
            {
              cardId: t.data.cardArray.card_id,
              cardExt:
                '{ "openid":"' +
                t.data.cardArray.openid +
                '", "timestamp":"' +
                t.data.cardArray.timestamp +
                '","nonce_str":"' +
                t.data.cardArray.nonceStr +
                '", "signature":"' +
                t.data.cardArray.signature +
                '"}'
            }
          ],
          success: function(t) {
            that.setData({
              flag: false
            });
            console.log(t);
            var a = t.data;
            console.log(a);
            app.util.request({
              url: "entry/wxapp/decryptcode",
              data: {
                code: t.cardList[0].code
              },
              cachetime: "30",
              success: function(t) {
                console.log(JSON.parse(t.data.content));
                code = JSON.parse(t.data.content).code;
                if (code != "") {
                  app.util.request({
                    url: "entry/wxapp/insCard",
                    data: {
                      card_id: i,
                      code: code,
                      card_on: n,
                      card_type: r,
                      iid: d
                    },
                    cachetime: "30",
                    success: function(t) {
                      that.setData({
                        flag: false
                      });
                      that.show(t.data.msg);
                      that.getCardList();
                    }
                  });
                }
              },
              fail: function(t) {
                console.log("decrypte failed", t.data);
              }
            });
          },
          fail: function(t) {
            that.setData({
              flag: false
            });
            console.log("add card failed", t);
          },
          completed: function(t) {
            console.log(t.data);
          }
        });
      }
    });
  },

  /**
   * 显示提示信息
   */
  show: function(t) {
    var a = this;
    this.setData({
      "toast.isHide": !0,
      "toast.content": t
    }),
      setTimeout(function() {
        a.setData({
          "toast.isHide": !1
        });
      }, 2e3);
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function() {},

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function() {},

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function() {},

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function() {},

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function() {},

  /**
   * Called when page reach bottom
   */
  onReachBottom: function() {},

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function() {}
});
