var app = getApp(), t = require("../../template/Toast.js");

Page({
    data: {
        loginStatus: "",
        menlist: [ {
            typeid: "onlinecheck",
            name: "在线买单记录"
        }, {
            typeid: "recharge_mcard",
            name: "会员卡充值记录"
        } ],
        active: {
            typeid: "",
            showMore: "",
            iSempty: "",
            data: [],
            erro: "没有记录"
        },
        page: 1,
        logdetails: {
            isHide: !1,
            typeid: "",
            payStatus: "",
            payFee: "",
            payType: "",
            payOn: "",
            cardDiscount: "",
            mcardDiscount: "",
            payTotal: "",
            payGive: "",
            payNote: "",
            payAddtime: "",
            payTime: ""
        }
    },
    onLoad: function(a) {
        var e = this, i = a.typeid ? a.typeid : "onlinecheck";
        setTimeout(function() {
            wx.setNavigationBarColor({
                frontColor: "#ffffff",
                backgroundColor: "#fda78e",
                navigationBarTextStyle: "white",
                animation: {
                    duration: 400,
                    timingFunc: "easeIn"
                }
            }), wx.setNavigationBarTitle({
                title: "我的订单"
            });
        }, 100), new t.ToastPannel(), e.setData({
            "active.typeid": i
        }), e.GetuserInfo(), e.getpaylog(i);
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onReachBottom: function(a) {
        var t = this, e = t.data.active.typeid;
        "1" == t.data.active.showMore ? t.getpaylog(e) : t.show("数据已加载完毕！");
    },
    GetuserInfo: function(a) {
        var t = this;
        t.data.loginStatus;
        app.util.getUserInfo(function(a) {
            t.setData({
                userInfo: a
            });
        });
    },
    changeFilter: function(a) {
        this.setData({
            active: {
                typeid: a.target.id,
                iSempty: "",
                showMore: "",
                data: []
            },
            page: 0
        }), this.getpaylog(a.target.id);
    },
    getpaylog: function(a) {
        var t = this, e = a, i = t.data.page;
        app.util.request({
            url: "entry/wxapp/getmypaylog",
            data: {
                typeid: e,
                pages: i
            },
            method: "POST",
            cachetime: "0",
            success: function(a) {
                t.setData({
                    page: parseInt(t.data.page) + 1,
                    "active.data": t.data.active.data.concat(a.data.data),
                    "active.showMore": a.data.showMore,
                    "active.iSempty": a.data.iSempty
                });
            }
        });
    },
    getpaylogdetails: function(a) {
        var t = this, e = a.currentTarget.dataset.oid;
        app.util.request({
            url: "entry/wxapp/getpaylogdetails",
            data: {
                oid: e
            },
            method: "POST",
            cachetime: "0",
            success: function(a) {
                console.log(a), 1 == a.data.status ? t.setData({
                    "logdetails.isHide": !0,
                    "logdetails.typeid": t.data.active.typeid,
                    "logdetails.payStatus": a.data.data.payStatus,
                    "logdetails.payFee": a.data.data.payFee,
                    "logdetails.payType": a.data.data.payType,
                    "logdetails.payOn": a.data.data.payOn,
                    "logdetails.cardDiscount": a.data.data.cardDiscount,
                    "logdetails.mcardDiscount": a.data.data.mcardDiscount,
                    "logdetails.payTotal": Number(a.data.data.payTotal).toFixed(2),
                    "logdetails.payGive": a.data.data.payGive,
                    "logdetails.payNote": a.data.data.payNote,
                    "logdetails.payAddtime": a.data.data.payAddtime,
                    "logdetails.payTime": a.data.data.payTime
                }) : t.show("获取详情失败！");
            }
        });
    },
    econfirm: function() {
        this.setData({
            "logdetails.isHide": !1
        });
    }
});