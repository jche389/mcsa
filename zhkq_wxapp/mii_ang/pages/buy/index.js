var app = getApp(), t = require("../../template/Toast.js");

Page({
    data: {
        payType: "",
        iSmcard: "",
        iSpay: !0,
        cardNum: "",
        tmoney: "",
        Gcard: {
            isHide: !1,
            animationData: {},
            cardList: []
        },
        balance: "",
        mcard_discount: "",
        mcard_on: "",
        mcard_fee: "",
        paymoney: "",
        card_on: "",
        card_type: "",
        card_title: "",
        card_discount: "",
        card_fee: "",
        card_minus: "",
        note: "",
        paySuccesslayer: {
            isHide: !1,
            payStatus: "",
            data: {
                payFee: "",
                payType: "",
                payOn: "",
                cardDiscount: "",
                mcardDiscount: "",
                mcardBalance: "",
                payTotal: "",
                payTime: ""
            }
        }
    },
    onLoad: function(a) {
        new t.ToastPannel(), setTimeout(function() {
            wx.setNavigationBarColor({
                frontColor: "#ffffff",
                backgroundColor: "#fda78e",
                navigationBarTextStyle: "white",
                animation: {
                    duration: 400,
                    timingFunc: "easeIn"
                }
            });
        }), wx.setNavigationBarTitle({
            title: "在线买单"
        }), this.GetuserInfo(), this.PageonLoad();
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {
        this.onLoad();
    },
    onReachBottom: function() {},
    onShareAppMessage: function() {},
    PageonLoad: function() {
        var t = this;
        app.util.request({
            url: "entry/wxapp/onlinecheck",
            data: {},
            method: "POST",
            cachetime: "30",
            success: function(a) {
                console.log(a), t.setData({
                    iSmcard: "1" == a.data.iSmcard,
                    balance: "1" == a.data.status ? a.data.balance : "0.00",
                    mcard_discount: a.data.discount,
                    mcard_on: a.data.mcard_on
                });
            }
        });
    },
    complete: function(a) {
        var t = this;
        t.cleanpayType(), t.clearusecardData();
        var e = a.detail.value;
        if (isNaN(e)) return t.show("消费金额应为数字"), !1;
        "" != e && t.setData({
            tmoney: Number(e).toFixed(2)
        });
        t.data.payType;
        t.GetCard(e), t.setData({
            iSpay: !0
        });
    },
    GetCard: function(a) {
        var t = this, e = a;
        app.util.request({
            url: "entry/wxapp/Moneyinputcomplete",
            data: {
                money: e
            },
            method: "POST",
            cachetime: "0",
            success: function(a) {
                t.setData({
                    "Gcard.cardList": a.data.cardList ? a.data.cardList : "",
                    cardNum: a.data.cardNum ? a.data.cardNum : "0"
                });
            }
        });
    },
    note: function(a) {
        this.setData({
            note: a.detail.value
        });
    },
    payType: function(a) {
        var t = this, e = t.data.tmoney;
        t.data.balance;
        if ("" == e) return t.show("请先输入消费金额"), !1;
        var n = a.currentTarget.dataset.type, c = a.currentTarget.dataset.dis;
        if ("mcard" == n) {
            if ("disabled" == c) return wx.showModal({
                title: "激活会员卡",
                content: "您的会员卡未激活是否前往激活",
                success: function(a) {
                    a.confirm ? wx.navigateTo({
                        url: "/mii_ang/pages/membercard/index"
                    }) : a.cancel;
                }
            }), !1;
            t.setData({
                payType: n
            }), t.statistics(e, n);
        } else t.setData({
            payType: n,
            mcard_fee: ""
        }), t.statistics(e, n);
    },
    showcard: function() {
        var a = this, t = wx.createAnimation({
            duration: 300,
            timingFunction: "ease-in-out"
        });
        (a.animation = t).translateY(-300).step(), a.setData({
            "Gcard.animationData": a.animation.export(),
            "Gcard.isHide": !0
        });
    },
    cancelcard: function(a) {
        var t = a, e = this, n = wx.createAnimation({
            duration: 300,
            timingFunction: "ease-out"
        });
        (e.animation = n).translateY(0).step(), e.setData({
            "Gcard.animationData": e.animation.export()
        }), setTimeout(function() {
            e.setData({
                "Gcard.isHide": !1
            });
        }, 300), "true" != t && e.clearusecardData(), e.cleanpayType();
    },
    clearusecardData: function() {
        this.setData({
            card_on: "",
            card_type: "",
            card_title: "",
            card_discount: "",
            card_minus: "",
            card_fee: ""
        });
    },
    getcard: function(a) {
        var t = this, e = t.data.cardNum;
        if ("" == e || "0" == e) return t.show("抱歉没有可用的卡券"), !1;
        t.showcard();
    },
    usecard: function(a) {
        var t = a.currentTarget.dataset.oid, e = a.currentTarget.dataset.tid, n = a.currentTarget.dataset.title, c = a.currentTarget.dataset.minus, r = a.currentTarget.dataset.discount;
        return this.setData({
            card_on: t,
            card_type: e,
            card_title: n,
            card_discount: r,
            card_minus: c
        }), this.cancelcard("true"), "";
    },
    GetuserInfo: function(a) {
        var t = this;
        app.util.getUserInfo(function(a) {
            t.setData({
                userInfo: a
            });
        });
    },
    cleanpayType: function() {
        this.setData({
            payType: "",
            paymoney: "",
            mcard_fee: ""
        });
    },
    cardDiscount: function(a) {
        var t = this, e = a, n = t.data.card_type, c = (t.data.mcard_discount, t.data.card_discount), r = t.data.card_minus;
        if ("GENERAL_COUPON" == n) {
            if ("" == r) return Number(e).toFixed(2);
            var o = parseFloat(e) - parseFloat(r);
            return t.setData({
                card_fee: Number(parseFloat(r)).toFixed(2)
            }), Number(o).toFixed(2);
        }
        if ("DISCOUNT" == n) {
            if ("" == c || parseFloat(c) < 1 || 10 < parseFloat(c)) return Number(e).toFixed(2);
            var i = e - (o = e * (10 * parseFloat(c) / 100));
            return t.setData({
                card_fee: Number(parseFloat(i)).toFixed(2)
            }), Number(o).toFixed(2);
        }
        return t.setData({
            card_fee: 0
        }), Number(e).toFixed(2);
    },
    statistics: function(a, t) {
        var e = this, n = a;
        if ("" == n) return e.show("请输入消费金额"), "";
        var c = t, r = e.cardDiscount(n);
        if ("wechat" == c) return e.setData({
            paymoney: r
        }), !1;
        if ("mcard" == c) {
            var o = e.data.mcard_discount;
            if ("" == o || parseFloat(o) < 1 || 10 < parseFloat(o)) return e.setData({
                paymoney: r
            }), !1;
            var i = r * (10 * parseFloat(o) / 100), s = r - Number(i).toFixed(2);
            e.setData({
                paymoney: Number(i).toFixed(2),
                mcard_fee: Number(s).toFixed(2)
            });
        } else e.setData({
            paymoney: n
        });
    },
    _pay_: function(a) {
        var e = this, t = parseFloat(e.data.tmoney), n = e.data.payType, c = parseFloat(e.data.paymoney), r = e.data.card_on, o = e.data.mcard_on, i = e.data.balance, s = e.data.card_fee, d = e.data.mcard_fee, u = e.data.note;
        return "disabled" == a.currentTarget.dataset.dis ? (e.show("请勿重复提交"), e.setData({
            iSpay: !0
        }), !1) : (e.setData({
            iSpay: !1
        }), "" == c ? (e.show("请输入消费金额"), e.setData({
            iSpay: !0
        }), !1) : "" == n ? (e.show("请选择支付方式"), e.setData({
            iSpay: !0
        }), !1) : "wechat" != n && "mcard" != n ? (e.show("支付类型错误"), e.setData({
            iSpay: !0
        }), !1) : "mcard" == n && c > parseFloat(i) ? (wx.showModal({
            title: "会员卡余额不足",
            content: "会员卡余额不足，是否前往充值?",
            success: function(a) {
                a.confirm ? wx.navigateTo({
                    url: "/mii_ang/pages/membercard/index"
                }) : a.cancel && e.setData({
                    iSpay: !0
                });
            }
        }), !1) : isNaN(c) || c < .01 ? (e.show("支付金额应大于 0 "), e.setData({
            iSpay: !0
        }), !1) : "" == u ? (e.show("请填写消费备注"), e.setData({
            iSpay: !0
        }), !1) : void app.util.request({
            url: "entry/wxapp/onlinecheckpay",
            data: {
                tmoney: t,
                payType: n,
                mcard_fee: d,
                mcard_on: o,
                card_fee: s,
                paymoney: c,
                card_on: r,
                note: u
            },
            method: "POST",
            cachetime: "0",
            success: function(a) {
                console.log(a);
                var t = a.data.oid;
                "wechat" == n ? wx.requestPayment({
                    timeStamp: a.data.payInfo.timeStamp,
                    nonceStr: a.data.payInfo.nonceStr,
                    package: a.data.payInfo.package,
                    signType: a.data.payInfo.signType,
                    paySign: a.data.payInfo.paySign,
                    success: function(a) {
                        e.onlinecheckcomplete(t);
                    },
                    fail: function(a) {
                        return e.show("取消支付或支付金额过大"), e.setData({
                            iSpay: !0
                        }), !1;
                    },
                    complete: function(a) {
                        console.log(a);
                    }
                }) : "mcard" == n && wx.showModal({
                    title: "会员卡支付",
                    content: "使用会员卡支付 " + e.data.paymoney + " 元",
                    success: function(a) {
                        a.confirm ? e.onlinecheckcomplete(t) : a.cancel && (e.setData({
                            iSpay: !0
                        }), e.show("成功取消支付"));
                    }
                });
            }
        }));
    },
    onlinecheckcomplete: function(a) {
        var t = this, e = a, n = t.data.payType;
        app.util.request({
            url: "entry/wxapp/onlinecheckcomplete",
            data: {
                oid: e,
                payType: n
            },
            method: "POST",
            cachetime: "0",
            success: function(a) {
                return console.log(a), "1" == a.data.status ? (t.cleanpayType(), t.clearusecardData(), 
                t.GetCard(t.data.tmoney), t.PageonLoad(), t.setData({
                    "paySuccesslayer.isHide": 1 == a.data.odata.payStatus,
                    "paySuccesslayer.payStatus": a.data.odata.payStatus,
                    "paySuccesslayer.data.payType": a.data.odata.payType,
                    "paySuccesslayer.data.payFee": Number(a.data.odata.payFee).toFixed(2),
                    "paySuccesslayer.data.payOn": a.data.odata.payOn,
                    "paySuccesslayer.data.cardDiscount": a.data.odata.cardDiscount,
                    "paySuccesslayer.data.mcardDiscount": a.data.odata.mcardDiscount,
                    "paySuccesslayer.data.mcardBalance": a.data.odata.mcardBalance,
                    "paySuccesslayer.data.payTotal": t.data.tmoney,
                    "paySuccesslayer.data.payTime": a.data.odata.payTime
                })) : (t.show(a.data.msg), t.PageonLoad()), t.setData({
                    iSpay: !0
                }), !1;
            }
        });
    },
    econfirm: function() {
        this.setData({
            "paySuccesslayer.isHide": !1
        });
    }
});