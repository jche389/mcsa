var app = getApp();

Page({
    data: {
        ImagesList: [],
        techInfo: {},
        attachurl: "",
        fabulous: 0,
        tid: "",
        thispath: ""
    },
    onLoad: function(t) {
        var a = this, e = t.tid;
        if ("" == e || null == e ? a.show("错误参数") : a.setData({
            tid: e,
            thispath: a.route
        }), app.util.footer(a), s = wx.getStorageSync("t_key")) {
            var o = s[e];
            this.setData({
                collection: o
            });
        } else {
            var s;
            (s = {})[e] = !1, wx.setStorageSync("t_key", s);
        }
        app.util.request({
            url: "entry/wxapp/gettechdetails",
            data: {
                tid: e
            },
            cachetime: "0",
            success: function(t) {
                a.setData({
                    techInfo: t.data.techInfo,
                    ImagesList: t.data.imagelist,
                    attachurl: t.data.attachurl,
                    fabulous: parseInt(t.data.techInfo.fabulous)
                }), setTimeout(function() {
                    wx.setNavigationBarColor({
                        frontColor: "#ffffff",
                        backgroundColor: "#fda78e",
                        animation: {
                            duration: 400,
                            timingFunc: "easeIn"
                        }
                    }), wx.setNavigationBarTitle({
                        title: t.data.title
                    });
                }, 100);
            }
        });
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onShareAppMessage: function(t) {
        var a = this;
        return "button" === t.from && console.log(t.target), {
            title: a.data.techInfo.tname + "--详情页",
            path: "/" + a.data.thispath + "?tid=" + a.data.tid,
            success: function(t) {
                a.show("感谢分享");
            },
            fail: function(t) {
                a.show("分享取消");
            }
        };
    },
    Imgshow: function(t) {
        var a = t.target.dataset.src;
        wx.previewImage({
            current: a,
            urls: this.data.ImagesList
        });
    },
    show: function(t) {
        var a = this;
        this.setData({
            "toast.isHide": !0,
            "toast.content": t
        }), setTimeout(function() {
            a.setData({
                "toast.isHide": !1
            });
        }, 2e3);
    },
    book: function(t) {
        var a = t.currentTarget.dataset.url;
        wx.navigateTo({
            url: a
        });
    },
    fabulous: function() {
        var a = this, e = wx.getStorageSync("t_key");
        e[a.data.tid] ? a.show("你已点赞！") : app.util.request({
            url: "entry/wxapp/fabulous",
            data: {
                tid: a.data.tid
            },
            cachetime: "0",
            success: function(t) {
                console.log(t), "1" == t.data.status ? (e[a.data.tid] = !0, wx.setStorageSync("t_key", e), 
                a.setData({
                    collection: !0
                }), a.setData({
                    fabulous: a.data.fabulous + 1
                }), a.show("感谢您的点赞")) : a.show(t.data.msg);
            }
        });
    }
});