var _request = require("../../util/request.js"), _request2 = _interopRequireDefault(_request);

function _interopRequireDefault(t) {
    return t && t.__esModule ? t : {
        default: t
    };
}

var app = getApp();

Page({
    data: {
        techlist: [],
        page: 1,
        active: {
            data: []
        },
        attachurl: "",
        show: "true",
        thispath: "",
        tabBarData: {
            thisurl: "",
            borderStyle: "#d9d9d9",
            backgroundColor: "#fff",
            list: []
        }
    },
    onLoad: function(t) {
        var a = this;
        app.util.footer(a);
        var e = null == t.typeid ? "all" : t.typeid;
        a.setData({
            thispath: a.route,
            "tabBarData.thisurl": a.route
        }), app.util.request({
            url: "entry/wxapp/techinfo",
            data: {},
            cachetime: "30",
            success: function(t) {
                a.setData({
                  menlist: t.data.menlist,
                  active: {
                    typeid: null == e ? "all" : e,
                    showMore: !0,
                    data: []
                  },
                  attachurl: t.data.attachurl
                }),a.gettechlist(e),setTimeout(
                 function () {
                  wx.setNavigationBarColor({
                    frontColor: "#000000",
                    backgroundColor: "#fff",
                  }), wx.setNavigationBarTitle({
                    title: t.data.title
                  });
                },100);
              }
        }),a.GettabBarData();
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onShareAppMessage: function(t) {
        var a = this;
        return "button" === t.from && console.log(t.target), {
            title: "",
            path: "/" + a.data.thispath,
            success: function(t) {
                a.show("感谢分享");
            },
            fail: function(t) {
                a.show("分享取消");
            }
        };
    },
    onPullDownRefresh: function() {
        this.setData({
            loading: !0,
            "active.data": [],
            "active.showMore": !0,
            "active.remind": "上滑加载更多",
            page: 0
        });
    },
    onReachBottom: function() {
        var t = this;
        "true" == t.data.show ? t.gettechlist(t.data.active.typeid) : t.show("数据已加载完毕！");
    },
    changeFilter: function(t) {
        this.setData({
            active: {
                typeid: t.target.id,
                data: [],
                showMore: !0,
                remind: "上滑加载更多"
            },
            page: 1
        }), this.gettechlist(t.target.id);
    },
    showtech: function(t){
      var a=t.currentTarget.dataset.url;
      wx.navigateTo({
        url: a,
      })
    },
    show: function(t) {
        var a = this;
        this.setData({
            "toast.isHide": !0,
            "toast.content": t
        }), setTimeout(function() {
            a.setData({
                "toast.isHide": !1
            });
        }, 2e3);
    },
    gettechlist: function(t) {
        var a = this, e = {
            tid: t || "all",
            page: a.data.page
        };
        _request2.default.get("gettechlist", e).then(function(t) {
            console.log(t), a.setData({
                "active.data": a.data.active.data.concat(t.tech),
                page: parseInt(a.data.page) + 1,
                show: t.show
            });
        }, function(t) {});
    },
    GettabBarData: function() {
        var a = this;
        app.util.request({
            url: "entry/wxapp/TabBarData",
            data: {},
            cachetime: "20",
            success: function(t) {
                a.setData({
                    "tabBarData.list": t.data.tabBarData
                });
            }
        });
    }
});