var app = getApp();

Page({
    data: {
      userInfo: {},
      nocard: true,
      tabBarData: {
        thisurl: "",
        borderStyle: "#d9d9d9",
        backgroundColor: "#fff",
        list: []
      },
    },
    onLoad: function (options) {
      this.GetTabBarData();
      wx.setNavigationBarTitle({
        title: "M C S A"
      });
      wx.setNavigationBarColor({
        frontColor: '#ffffff',
        backgroundColor: '#4A4A4A'
      });
    },
    GetTabBarData: function () {
      var a = this;
      app.util.request({
        url: "entry/wxapp/TabBarData",
        data: {},
        cachetime: "30",
        success: function (t) {
          a.setData({
            "tabBarData.list": t.data.tabBarData
          });
        }
      });
    },
   
});