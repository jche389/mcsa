function _defineProperty(t, a, e) {
  return (
    a in t
      ? Object.defineProperty(t, a, {
          value: e,
          enumerable: !0,
          configurable: !0,
          writable: !0
        })
      : (t[a] = e),
    t
  );
}

var _ca = require("../../public/cache.js"),
  app = getApp();

Page({
  data: {
    text: "微思出品",
    buttonDisable: false,
    active: {
      type: "cardall",
      url: "entry/wxapp/getcardlist"
    },
    nocard: !1,
    toast: {
      isHide: !1,
      content: ""
    },
    input: {
      isHide: !1,
      uname: "",
      uphone: ""
    },
    uname: "",
    uphone: "",
    loading: "show",
    onShow: "",
    ad: [],
    webInfo: {},
    men: {},
    menuData: [],
    footerData: [],
    hotgoods: {},
    attachurl: "",
    toad: {
      isHide: !1,
      title: "",
      images: "",
      url: "",
      info: "",
      path: ""
    },
    toast: {
      isHide: !1,
      content: ""
    },
    thispath: "",
    swiperCurrent: "",
    tabBarData: {
      thisurl: "",
      borderStyle: "#d9d9d9",
      backgroundColor: "#fff",
      list: []
    }
  },
  /**
   * 获取用户信息并进入主界面
   */
  bindGetUserInfo: function(t) {
    var that = this;
    wx.redirectTo({
      url: "../home/home"
    });
    console.log(t.detail.userInfo);
    if (!that.data.buttonDisable) {
      that.setData({ buttonDisable: true });
      wx.login({
        success: function(res) {
          console.log(res);
          var code = res.code;
          wx.getUserInfo({
            success: function(res) {
              console.log(res);
              var userInfo = res.userInfo;
              var nickName = userInfo.nickName;
              var avatarUrl = userInfo.avatarUrl;
              var gender = userInfo.gender; //性别 0：未知、1：男、2：女
              var province = userInfo.province;
              var city = userInfo.city;
              var country = userInfo.country;
              that.setData({
                avatarUrl: userInfo.avatarUrl,
                nickName: userInfo.nickName
              });
              app.util.request({
                url: "entry/wxapp/openid/zh_hdbm",
                cachetime: "0",
                data: { code: code },
                success: function(res) {
                  console.log(res);
                  var openid = res.data.openid;
                  that.setData({
                    openid: res.data.openid
                  });
                  wx.setStorageSync("key", res.data.session_key);
                  wx.setStorageSync("openid", res.data.openid);
                  wx.setStorageSync("img", avatarUrl);
                  wx.setStorageSync("name", nickName);
                  console.log(res.data);
                  // 报名用户信息
                  app.util.request({
                    url: "entry/wxapp/login/zh_hdbm",
                    headers: {
                      "Content-Type": "application/json"
                    },
                    cachetime: "0",
                    data: { openid: openid, img: avatarUrl, name: nickName },
                    success: function(res) {
                      console.log(res);
                      wx.setStorageSync("uid_bm", res.data.id);
                      wx.setStorageSync("user_bm", res.data);
                      that.setData({
                        uid: res.data.id,
                        loading: true
                      });
                    },
                    fail: function() {
                      console.log("失败了");
                    }
                  });
                  // 卡券用户信息
                  app.util.request({
                    url: "entry/wxapp/login",
                    headers: {
                      "Content-Type": "application/json"
                    },
                    cachetime: "0",
                    data: {
                      openid: openid
                    },
                    success: function(res) {
                      console.log(res);
                      wx.setStorageSync("uid_coupon", res.data[0].uid);
                      wx.setStorageSync("user_coupon", res.data);
                    },
                    fail: function() {
                      console.log("获取卡券用户失败");
                    }
                  });
                  // 前往主页
                },
                fail: function() {
                  console.log("获取用户信息失败");
                  wx: wx.showToast({
                    title: "获取信息失败，建议重新启动小程序",
                    icon: "",
                    image: "",
                    duration: 3000,
                    mask: true,
                    success: function(res) {},
                    fail: function(res) {},
                    complete: function(res) {}
                  });
                  that.setData({ buttonDisable: false });
                }
              });
            },
            fail: function() {
              console.log("失败了");
              that.setData({ buttonDisable: false });
            }
          });
        }
      });
      // console.log(t);
    }
  },
  reload: function(e) {
    var that = this;
    // 获取平台设置的审核方式
    // app.util.request({
    //   'url': 'entry/wxapp/setup',
    //   'cachetime': '0',
    //   success: function (res) {
    //     console.log('获取平台设置的审核方式')
    //     console.log(res)
    //   }
    // })
    var user_id = wx.getStorageSync("uid");
    // console.log(user_id)
    function getNowFormatDate() {
      var date = new Date();
      var seperator1 = "-";
      var seperator2 = ":";
      var month = date.getMonth() + 1;
      var strDate = date.getDate();
      if (month >= 1 && month <= 9) {
        month = "0" + month;
      }
      if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
      }
      var currentdate =
        date.getFullYear() +
        seperator1 +
        month +
        seperator1 +
        strDate +
        " " +
        date.getHours() +
        seperator2 +
        date.getMinutes() +
        seperator2 +
        date.getSeconds();
      return currentdate;
    }
    var time1 = getNowFormatDate();
    var time = getNowFormatDate().slice(0, 16);
    wx.setStorageSync("time", time1);
    // console.log(time)
    app.util.request({
      url: "entry/wxapp/hxCode",
      cachetime: "0",
      data: { user_id: user_id },
      success: function(res) {
        // console.log(res)
        that.setData({
          bath: res.data
        });
      }
    });
  },
  onLoad: function() {
    var that = this;
    wx.setNavigationBarTitle({
      title: "我 的 档 案"
    });
    wx.setNavigationBarColor({
      frontColor: "#ffffff",
      backgroundColor: "#922527"
    });
    // that.reload()
  },
  changemen: function(t) {
    this.setData({
      cardList: ""
    }),
      this.setData({
        active: {
          type: t.target.dataset.type,
          url: t.target.dataset.url
        }
      }),
      this.Getcardlist();
  },
  onReady: function() {
    var t = this;
    setTimeout(function() {
      t.setData({
        loading: "hide"
      });
    }, 1e3);
  },
  onShow: function() {
    this.setData({
      onShow: !0,
      buttonDisable: false
    });
  },
  onPullDownRefresh: function() {
    this.onLoad();
  },
  onHide: function() {
    this.setData({
      onShow: !1
    });
  },
  onUnload: function() {
    this.setData({
      onShow: !1
    });
  },
  // GetuserInfo: function(t) {
  //   var a = this;
  //   app.util.getUserInfo(function(t) {
  //     a.setData({
  //       userInfo: t.wxInfo
  //     });
  //   });
  // },
  onShareAppMessage: function(t) {
    // var a = this;
    // return "button" === t.from && console.log(t.target), {
    //   // title: '微思科技WeBoost——全澳首家小程序本地专业开发',
    //   path: "/" + a.data.thispath,
    //   imageUrl: '/images/beautyShare.jpg',
    //   success: function(t) {
    //     a.show("感谢分享");
    //   },
    //   fail: function(t) {
    //     a.show("分享取消");
    //   }
    // };
  },
  openmap: function(t) {
    wx.openLocation({
      latitude: parseFloat(this.data.webInfo.lat),
      longitude: parseFloat(this.data.webInfo.lng),
      name: this.data.webInfo.companyname,
      address: this.data.webInfo.address,
      scale: 18
    });
  },
  navservice: function(t) {
    wx.navigateTo({
      url: t.currentTarget.dataset.url
    });
  },
  Jumpurl: function(t) {
    wx.navigateTo({
      url: t.currentTarget.dataset.url
    });
  },
  gobook: function(t) {
    wx.navigateTo({
      url: "/mii_ang/pages/book/index?gid=" + t.currentTarget.dataset.gid
    });
  },
  godetails: function(t) {
    wx.navigateTo({
      url:
        "0" != t.currentTarget.dataset.gid
          ? "/mii_ang/pages/detail/detail?gid=" + t.currentTarget.dataset.gid
          : "/mii_ang/pages/index/index"
    });
  },
  adclose: function(t) {
    this.setData({
      "toad.isHide": !1
    });
  },
  goto: function(t) {
    console.log(t),
      wx.navigateTo({
        url: t.currentTarget.dataset.url
      });
  },
  show: function(t) {
    var a = this;
    this.setData({
      "toast.isHide": !0,
      "toast.content": t
    }),
      setTimeout(function() {
        a.setData({
          "toast.isHide": !1
        });
      }, 2e3);
  },
  swiperChange: function(t) {
    this.setData({
      swiperCurrent: t.detail.current
    });
  }
});
