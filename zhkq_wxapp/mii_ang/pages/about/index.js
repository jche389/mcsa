var _templateObject = _taggedTemplateLiteral([ "" ], [ "" ]);

function _taggedTemplateLiteral(t, a) {
    return Object.freeze(Object.defineProperties(t, {
        raw: {
            value: Object.freeze(a)
        }
    }));
}

var app = getApp();

Page({
    data: {
      text: "微思出品",
        remind: "加载中",
        ImagesList: "",
        icon1: "/images/icon/address_2.png",
        icon2: "/images/icon/time.png",
        icon3: "/images/icon/phone.png",
        icon4: "/images/icon/live.png",
        icon5: "/images/icon/picture.png",
        showData: "",
        video: "",
        userInfo: {},
        attachurl: "",
        urls: ""
    },
    onLoad: function(t) {
        console.log(this);
        var a = this;
        app.util.request({
            url: "entry/wxapp/about",
            data: {},
            cachetime: "30",
            success: function(t) {
                console.log(t), a.setData({
                    showData: t.data.data,
                    ImagesList: t.data.data.pics,
                    urls: t.data.data.urls,
                    attachurl: t.data.data.attachurl
                }), setTimeout(function() {
                    a.setData({
                        video: t.data.data.videourl
                    });
                }, 3e3), setTimeout(function() {
                    wx.setNavigationBarColor({
                        frontColor: "#ffffff",
                        backgroundColor: "#fda78e",
                        navigationBarTextStyle: "white",
                        animation: {
                            duration: 400,
                            timingFunc: "easeIn"
                        }
                    }), wx.setNavigationBarTitle({
                        title: t.data.title
                    });
                }, 100);
            }
        });
    },
    onReady: function() {
        app.util.footer(this);
        var t = this;
        setTimeout(function() {
            t.setData({
                remind: ""
            });
        }, 3e3);
    },
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    tel: function(t) {
        wx.makePhoneCall({
            phoneNumber: this.data.showData.phone
        });
    },
    Imgshow: function(t) {
        console.log(t);
        var a = t.currentTarget.dataset.src;
        wx.previewImage({
            current: a,
            urls: this.data.urls
        });
    },
    Getadd: function(t) {
        wx.openLocation({
            latitude: parseFloat(this.data.showData.lat),
            longitude: parseFloat(this.data.showData.lng),
            name: this.data.showData.companyname,
            address: this.data.showData.address,
            scale: 18
        });
    },
    onPullDownRefresh: function() {
        var a = this;
        app.util.request({
            url: "entry/wxapp/about",
            data: {},
            cachetime: "30",
            success: function(t) {
                a.setData({
                    showData: t.data.data,
                    ImagesList: t.data.data.pics(_templateObject)
                }), setTimeout(function() {
                    a.setData({
                        video: t.data.data.videourl
                    });
                }, 3e3), setTimeout(function() {
                    wx.stopPullDownRefresh();
                }, 1500);
            }
        });
    },
    onShareAppMessage: function(t) {
        return "button" === t.from && console.log(t.target), {
            title: this.data.showData.companyname,
            path: "mii_ang/pages/index/index",
            success: function(t) {
                wx.showToast({
                    title: "分享成功",
                    icon: "success",
                    duration: 2e3
                });
            },
            fail: function(t) {
                wx.showToast({
                    title: "分享失败",
                    icon: "loading",
                    duration: 2e3
                });
            }
        };
    },
    goindex: function() {
        wx.redirectTo({
            url: "/mii_ang/pages/index/index"
        });
    }
});