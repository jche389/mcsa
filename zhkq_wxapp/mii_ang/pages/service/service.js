var _request = require("../../util/request.js"), _request2 = _interopRequireDefault(_request);

function _interopRequireDefault(t) {
    return t && t.__esModule ? t : {
        default: t
    };
}

var app = getApp();

Page({
    data: {
        page: 1,
        menlist: {},
        active: [],
        attachurl: "",
        info: {},
        show: "true",
        thispath: "",
        tabBarData: {
            thisurl: "",
            borderStyle: "#d9d9d9",
            backgroundColor: "#fff",
            list: []
        }
    },
    onLoad: function(t) {
        var a = this;
        app.util.footer(a);
        var e = null == t.typeid ? "all" : t.typeid;
        a.setData({
            thispath: a.route,
            "tabBarData.thisurl": a.route + "?typeid=" + e
        }), app.util.request({
            url: "entry/wxapp/serviceinfo",
            data: {
                uniacid: app.siteInfo.uniacid
            },
            cachetime: "30",
            success: function(t) {
              console.log(t);
                a.setData({
                    menlist: t.data.menlist,
                    active: {
                        typeid: null == e ? "all" : e,
                        showMore: !0,
                        data: []
                    },
                    attachurl: t.data.attachurl
                }), a.getGoodsList(e), setTimeout(function() {
                    wx.setNavigationBarColor({
                        frontColor: "#000000",
                        backgroundColor: "#FFFFFF",
                        animation: {
                            duration: 400,
                            timingFunc: "easeIn"
                        }
                    }), wx.setNavigationBarTitle({
                        title: t.data.title
                    });
                }, 100);
            }
        }), a.GettabBarData();
    },
    onShow: function() {},
    onPullDownRefresh: function() {
        this.setData({
            loading: !0,
            "active.data": [],
            "active.showMore": !0,
            "active.remind": "上滑加载更多",
            page: 1
        });
    },
    onReachBottom: function() {
        var t = this;
        "true" == t.data.show ? t.getGoodsList(t.data.active.typeid) : t.show("已经是底线！");
    },
    getGoodsList: function(t) {
        var a = this, e = t || "all";
        app.util.request({
            url: "entry/wxapp3/test",
            data: {
                tid: e,
                page: a.data.page
            },
            cachetime: "0",
            success: function(t) {
              console.log(t);
                a.setData({
                    page: a.data.page + 1,
                    "active.data": a.data.active.data.concat(t.data.goods),
                    show: t.data.show
                });
            },
            fail: function(t) {}
        });
    },
    changeFilter: function(t) {
        this.setData({
            active: {
                typeid: t.target.id,
                data: [],
                showMore: !0,
                remind: "上滑加载更多"
            },
            page: 0
        }), this.getGoodsList(t.target.id);
    },
    changeFilterDisabled: function() {
        var t = this;
        t.data.disabledRemind || (t.setData({
            disabledRemind: !0
        }), setTimeout(function() {
            t.setData({
                disabledRemind: !1
            });
        }, 2e3));
    },
    godetails: function(t) {
        wx.navigateTo({
            url: t.currentTarget.dataset.urls
        });
    },
    gobook: function(t) {
        wx.navigateTo({
            url: t.currentTarget.dataset.url
        });
    },
    show: function(t) {
        var a = this;
        this.setData({
            "toast.isHide": !0,
            "toast.content": t
        }), setTimeout(function() {
            a.setData({
                "toast.isHide": !1
            });
        }, 2e3);
    },
    onShareAppMessage: function(t) {
        var a = this;
        return "button" === t.from && console.log(t.target), {
            title: "服务项目",
            path: "/" + a.data.thispath,
            success: function(t) {
                a.show("感谢分享");
            },
            fail: function(t) {
                a.show("分享取消");
            }
        };
    },
    GettabBarData: function() {
        var a = this;
        app.util.request({
            url: "entry/wxapp/TabBarData",
            data: {},
            cachetime: "30",
            success: function(t) {
                a.setData({
                    "tabBarData.list": t.data.tabBarData
                });
            }
        });
    }
});