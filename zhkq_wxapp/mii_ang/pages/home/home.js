const app = getApp();

// mii_ang/pages/home/home.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    imgs: [
      { url: "../../images/icon/coupon.png" },
      { url: "../../images/coupon.png" },
      { url: "../../images/coupon.png" }
    ],
    activityList: [],
    swiperCurrent: 0,
    recentSwiperCurrent: 0,
    cardPageSize: 3,
    tabBarData: {
      thisurl: "mii_ang/pages/home/home",
      borderStyle: "#d9d9d9",
      backgroundColor: "#fff",
      list: []
    },
    url: "",
    active: {
      type: "cardall",
      url: "entry/wxapp/getcardlistwithlogo"
    }
  },

  /**
   * swiper change handler
   */
  activitySwiperChange: function(e) {
    this.setData({
      swiperCurrent: e.detail.current
      // currentDot: Math.ceil(e.detail.current / 3)
    });
  },

  /**
   * swiper change handler
   */
  cardSwiperChange: function(e) {
    this.setData({
      swiperCardCurrent: e.detail.current
    });
  },

  /**
   * 下一个滚动图片
   */
  nextSwiper: function(e) {
    var a = this;
    var next =
      a.data.swiperCurrent == a.data.activityList.length - 1
        ? 0
        : a.data.swiperCurrent + 1;
    this.setData({
      swiperCurrent: next
    });
  },

  /**
   * 上一个滚动图片
   */
  prevSwiper: function() {
    var a = this;
    var next =
      a.data.swiperCurrent == 0
        ? a.data.activityList.length - 1
        : a.data.swiperCurrent - 1;
    this.setData({
      swiperCurrent: next
    });
  },

  /**
   * 查看所有
   */
  viewAllDisc: function(t) {
    console.log(t);
    wx.navigateTo({
      url: "../coupons/coupons"
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    wx.setNavigationBarTitle({
      title: "蒙 纳 士 消 息"
    });
    wx.setNavigationBarColor({
      frontColor: "#ffffff",
      backgroundColor: "#922527"
    });
    wx.hideLoading();
    // 获取图片URL
    that.getUrl();
    // 加载底部菜单
    that.getTabBarData();
    // 获取用户信息
    that.getUserInfo();
    // 获取卡券
    that.getCardList();
    // 领取会员卡
    // that.addMemberCard();
    // 获取活动
    that.getActivityList();
    console.log(this.data.tabBarData);
  },

  /**
   * 获取底部菜单栏
   */
  getTabBarData: function() {
    var a = this;
    app.util.request({
      url: "entry/wxapp/TabBarData",
      data: {},
      cachetime: "30",
      success: function(t) {
        console.log("tabBar", t.data);
        a.setData({
          "tabBarData.list": t.data.tabBarData
        });
      }
    });
  },

  /**
   * 显示分类卡券
   */
  showCoupon: function(t) {
    var type = t.currentTarget.dataset.type;
    wx.navigateTo({
      url: "coupons/coupons",
      data: {
        type: type
      }
    });
  },

  /**
   * 活动详情
   */
  viewDetail: function(e) {
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    });
  },

  /**
   * 商家详情
   */
  viewCompany: function(e) {
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    });
  },

  /**
   * 获取用户信息
   */
  getUserInfo: function() {
    var a = this;
    console.log("Getting user information");
    app.util.getUserInfo(function(t) {
      a.setData({
        sessionid: t.sessionid
      });
    });
  },

  /**
   * 获取卡券信息
   */
  getCardList: function() {
    var a = this;
    console.log("Weboost-JL: a", a.data.active);
    app.util.request({
      url: a.data.active.url,
      data: {},
      cachetime: "10",
      success: function(t) {
        console.log(t.data);
        a.formateDate(t.data.cardlist);
        a.setData({
          allCards: t.data.cardlist
        });
        t.data.cardlist.length > 0
          ? t.data.cardlist.length < 5
            ? a.setData({
                popCardList: t.data.cardlist
              })
            : a.setData({
                popCardList: [
                  t.data.cardlist[0],
                  t.data.cardlist[1],
                  t.data.cardlist[2],
                  t.data.cardlist[3],
                  t.data.cardlist[4]
                ]
              })
          : a.setData({
              nocard: "1" == t.data.nocard
            });
      }
    });
  },

  /**
   * 领取卡券
   */
  wx_addcard: function(t) {
    // console.log('Weboost-JL: t', t);
    var that = this;
    // console.log('Weboost-JL: that', that);
    var i = t.currentTarget.dataset.cid,
      n = t.currentTarget.dataset.oid,
      r = t.currentTarget.dataset.tid,
      d = t.currentTarget.dataset.iid;
    app.util.request({
      url: "entry/wxapp/myaddcard",
      data: {
        card_id: i,
        cid: t.currentTarget.dataset.cid
      },
      cachetime: "30",
      success: function(t) {
        console.log(t.data);
        wx.addCard({
          cardList: [
            {
              cardId: t.data.cardArray.cardId,
              cardExt:
                '{"code": "", "openid":"' +
                t.data.cardArray.openid +
                '", "timestamp":"' +
                t.data.cardArray.timestamp +
                '","nonce_str":"' +
                t.data.cardArray.nonceStr +
                '", "signature":"' +
                t.data.cardArray.signature +
                '"}'
            }
          ],
          success: function(t) {
            console.log(t);
            var a = t.cardList;
            app.util.request({
              url: "entry/wxapp/decryptcode",
              data: {
                code: t.cardList[0].code
              },
              cachetime: "30",
              success: function(t) {
                console.log(t.data);
                var code = JSON.parse(t.data.content).code;
                app.util.request({
                  url: "entry/wxapp/insCard",
                  data: {
                    card_id: i,
                    code: code,
                    card_on: n,
                    card_type: r,
                    iid: d
                  },
                  cachetime: "30",
                  success: function(t) {
                    that.show(t.data.msg);
                    that.getCardList();
                  }
                });
                // wx.openCard({
                //   cardList: [{
                //     cardId: i,
                //     code: code
                //   }],
                //   success: function (t) {
                //     console.log("开卡成功");
                //   }
                // })
              }
            });
          },
          fail: function(t) {
            wx.hideLoading(), console.log(t);
          }
        });
      }
    });
  },

  /**
   * 日期格式转换
   */
  formateDate: function(cardList) {
    console.log(cardList);
    for (let card of cardList) {
      if (card.date.indexOf("DATE_TYPE_FIX_TERM") > -1)
        var dateInfo = JSON.parse(card.date);
      card.date = "自领取之日";
      // console.log(dateInfo);
      if (dateInfo) {
        card.date +=
          dateInfo.fixed_begin_term == 0
            ? "当日"
            : dateInfo.fixed_begin_term + "天后 ";
        card.date += dateInfo.fixed_term + "天内有效";
      }
    }
  },

  /**
   * 显示提示信息
   */
  show: function(t) {
    var a = this;
    this.setData({
      "toast.isHide": !0,
      "toast.content": t
    }),
      setTimeout(function() {
        a.setData({
          "toast.isHide": !1
        });
      }, 2e3);
  },

  /**
   * 卡券分页
   */
  // cardPaging: function(t) {
  //   var a = this;
  //   var cardList = t.data.cardlist;
  //   console.log(cardList);
  //   if (cardList.length <= a.data.cardPageSize) {
  //     a.setData({
  //       cardList: [cardList],
  //       nocard: "1" == t.data.nocard
  //     })
  //   } else {
  //     var index = 0;
  //     var page = 0;
  //     var newCardList = [];
  //     var subList = [];
  //     var subIndex = 0;
  //     for (index; index < cardList.length; index++) {
  //       if (cardList[index].card_type == "MEMBER_CARD") {
  //         continue;
  //       }
  //       if (subIndex == a.data.cardPageSize) {
  //         subList = [];
  //         subIndex = 0;
  //         subList[subIndex++] = cardList[index];
  //         newCardList[++page] = subList;
  //       } else {
  //         subList[subIndex++] = cardList[index];
  //         newCardList[page] = subList;
  //       }
  //     }
  //     a.setData({
  //       cardList: newCardList,
  //       nocard: "1" == t.data.nocard
  //     });
  //     console.log(a.data.cardList);
  //   }
  // },

  /**
   * 领取会员卡
   */
  addMemberCard: function() {
    var that = this;
    // 判断是否已经领取
    var noCard = true;
    app.util.request({
      url: "entry/wxapp/getmycardlist",
      data: {},
      cachetime: "10",
      success: function(t) {
        console.log(t.data.cardlist);
        for (let card of t.data.cardlist) {
          if (card.card_type == "MEMBER_CARD") {
            noCard = false;
          }
        }
        if (noCard) {
          var memberCard;
          var openid;
          console.log(that.data.allCards);
          // 领取会员卡
          if (that.data.allCards != undefined)
            for (let card of that.data.allCards) {
              if (card.card_type == "MEMBER_CARD") {
                console.log("nocard");
                console.log(card.card_id);
                app.util.request({
                  url: "entry/wxapp/myaddcard",
                  data: {
                    card_id: card.card_id
                  },
                  cachetime: "30",
                  success: function(t) {
                    console.log(t);
                    wx.addCard({
                      cardList: [
                        {
                          cardId: t.data.cardArray.cardId,
                          cardExt:
                            '{"code": "", "openid":"' +
                            t.data.cardArray.openid +
                            '", "timestamp":"' +
                            t.data.cardArray.timestamp +
                            '","nonce_str":"' +
                            t.data.cardArray.nonceStr +
                            '", "signature":"' +
                            t.data.cardArray.signature +
                            '"}'
                        }
                      ],
                      success: function(t) {
                        console.log(t);
                        var a = t.cardList;
                        app.util.request({
                          url: "entry/wxapp/insCard",
                          data: {
                            card_id: card.card_id,
                            code: a,
                            card_on: card.card_on,
                            card_type: card.card_type,
                            iid: card.id
                          },
                          cachetime: "30",
                          success: function(t) {
                            e.show(t.data.msg);
                          },
                          fail: function(t) {
                            console.log(t);
                          }
                        });
                      },
                      fail: function(t) {
                        wx.hideLoading(), console.log(t);
                      }
                    });
                  }
                });
              }
            }
        }
      }
    });
  },

  /**
   * 领取系统优惠卷
   */
  mii_addcard: function(t) {
    var a = this,
      e = t.currentTarget.dataset.cid,
      i = t.currentTarget.dataset.oid,
      n = t.currentTarget.dataset.tid,
      r = t.currentTarget.dataset.pid,
      d = t.currentTarget.dataset.iid,
      s = "cardList[" + r + "].card_on";
    app.util.request({
      url: "entry/wxapp/addcardAndmii",
      data: {
        iid: d,
        card_id: e,
        card_on: i,
        card_type: n
      },
      cachetime: "0",
      success: function(t) {
        console.log(t);
        "1" == t.data.status
          ? (a.setData(_defineProperty({}, s, t.data.card_on)),
            a.show(t.data.msg))
          : "2" == t.data.status
          ? (a.show(t.data.msg),
            setTimeout(function() {
              // a.bind();
            }, 1e3))
          : a.show(t.data.msg);
      }
    });
  },

  /**
   * 获取服务器路径
   */
  getUrl: function() {
    var that = this;
    app.util.request({
      url: "entry/wxapp/attachurl/zh_hdbm",
      headers: {
        "Content-Type": "application/json"
      },
      cachetime: "0",
      success: function(res) {
        var url = res.data;
        wx.setStorageSync("url", res.data);
        that.setData({
          url: url
        });
        console.log(res);
      }
    });
  },

  /**
   * 显示提示信息
   */
  show: function(t) {
    var a = this;
    this.setData({
      "toast.isHide": !0,
      "toast.content": t
    }),
      setTimeout(function() {
        a.setData({
          "toast.isHide": !1
        });
      }, 2e3);
  },

  /**
   * 获取活动
   */
  getActivityList: function() {
    var a = this;
    //活动的接口
    app.util.request({
      url: "entry/wxapp/activity/zh_hdbm",
      headers: {
        "Content-Type": "application/json"
      },
      cachetime: "0",
      success: function(res) {
        console.log(res);
        a.setData({
          activityList: res.data
        });
      }
    });
  },

  /**
   * 查看更多
   */
  seeMoreCoupons: function() {
    wx.navigateTo({
      url: "../coupons/coupons"
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {}
});
