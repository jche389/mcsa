var app = getApp(), t = require("../../template/Toast.js");

Page({
    data: {
        text: "我的",
        sid: "",
        mcard: "",
        level: "",
        loginStatus: "",
        input: {
            isHide: !1,
            uname: "",
            uphone: ""
        },
        pay: {
            isHide: !1,
            payNumber: ""
        },
        activation: "",
        activation_money: "",
        description: {},
        userInfo: {},
        paySuccesslayer: {
            isHide: !0
        },
        tabBarData: {
            thisurl: "",
            borderStyle: "#d9d9d9",
            backgroundColor: "#fff",
            list: []
        },
        thispath: ""
    },
    onLoad: function(a) {
        var e = this;
        new t.ToastPannel(), e.setData({
            thispath: e.route,
            "tabBarData.thisurl": e.route
        }), e.GetuserInfo(), setTimeout(function() {
            wx.setNavigationBarColor({
                frontColor: "#ffffff",
                backgroundColor: "#fda78e",
                navigationBarTextStyle: "white",
                animation: {
                    duration: 400,
                    timingFunc: "easeIn"
                }
            });
        }), wx.setNavigationBarTitle({
            title: "我的会员卡"
        }), e.Getmcardinfo(), e.GettabBarData();
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    Getmcardinfo: function() {
        var t = this;
        app.util.request({
            url: "entry/wxapp/mcardInfo",
            data: {},
            cachetime: "0",
            success: function(a) {
                console.log(a), 1 == a.data.status ? t.setData({
                    mcard: a.data.Resdata,
                    level: a.data.level,
                    activation: a.data.activation,
                    "input.uname": a.data.Resdata.name,
                    "input.uphone": a.data.Resdata.phone,
                    description: a.data.description,
                    activation_money: a.data.activation_money,
                    loginStatus: a.data.loginStatus
                }) : t.setData({
                    loginStatus: a.data.loginStatus
                });
            }
        });
    },
    GetuserInfo: function(a) {
        var t = this, e = t.data.loginStatus;
        app.util.getUserInfo(function(a) {
            t.setData({
                userInfo: a
            }), 1 != e && t.Getmcardinfo();
        });
    },
    activation: function() {
        this.data.activation;
        this.setData({
            "input.isHide": !0
        });
    },
    payclose: function() {
        this.setData({
            "pay.isHide": !1
        });
    },
    getPhoneNumber: function(t) {
        var e = this;
        wx.login({
            success: function(a) {
                "getPhoneNumber:ok" == t.detail.errMsg ? app.util.request({
                    url: "entry/wxapp/decode",
                    data: {
                        iv: t.detail.iv,
                        data: t.detail.encryptedData,
                        code: a.code
                    },
                    cachetime: "0",
                    success: function(a) {
                        "1" == a.data.status ? e.setData({
                            "input.uphone": a.data.phoneNumber
                        }) : e.show(a.data.msg);
                    }
                }) : e.show("获取失败,请允许系统获取您的手机号码");
            }
        });
    },
    uname: function(a) {
        this.setData({
            "input.uname": a.detail.value
        });
    },
    uphone: function(a) {
        return this.show("请点击右侧小图标获取"), !1;
    },
    save: function(a) {
        var n = this, i = n.data.input.uname, o = n.data.input.uphone;
        return "" == i ? (n.show("请输入姓名"), !1) : "" == o ? (n.show("请输入联系电话"), !1) : /^13[0-9]{9}$|14[0-9]{9}$|15[0-9]{9}$|18[0-9]{9}$|17[0-9]{9}$/.test(o) ? void app.util.request({
            url: "entry/wxapp/Binmember",
            data: {
                uname: i,
                uphone: o
            },
            method: "POST",
            cachetime: "0",
            success: function(t) {
                if ("1" == t.data.status) if ("pay" == t.data.type) {
                    var e = t.data.orderid;
                    t.data.uid;
                    wx.requestPayment({
                        timeStamp: t.data.payInfo.timeStamp,
                        nonceStr: t.data.payInfo.nonceStr,
                        package: t.data.payInfo.package,
                        signType: t.data.payInfo.signType,
                        paySign: t.data.payInfo.paySign,
                        success: function(a) {
                            app.util.request({
                                url: "entry/wxapp/paybindmember",
                                data: {
                                    orderid: e,
                                    uname: i,
                                    uphone: o
                                },
                                method: "POST",
                                cachetime: "0",
                                success: function(a) {
                                    "1" == a.data.status ? (n.close(), n.show(a.data.msg), setTimeout(function() {
                                        n.onLoad();
                                    }, 2e3)) : n.show(a.data.msg);
                                }
                            });
                        },
                        fail: function(a) {
                            n.show("取消支付");
                        },
                        complete: function(a) {
                            console.log(t);
                        }
                    });
                } else "ok" == t.data.type && (console.log(t), n.show(t.data.msg), setTimeout(function() {
                    n.onLoad(), n.close();
                }, 2e3)); else n.show(t.data.msg);
            }
        }) : (n.show("联系电话错误"), !1);
    },
    close: function(a) {
        this.setData({
            "input.isHide": !1
        }), this.Getmcardinfo();
    },
    payNumber: function(a) {
        this.setData({
            "pay.payNumber": a.detail.value
        });
    },
    recharge: function() {
        var e = this, a = e.data.pay.payNumber;
        return isNaN(a) ? (e.show("充值金额应为数字"), !1) : a <= 0 ? (e.show("充值金额不能小于等于零或为空"), 
        !1) : void app.util.request({
            url: "entry/wxapp/paymembercard",
            data: {
                payNumber: a
            },
            method: "POST",
            cachetime: "0",
            success: function(a) {
                if (console.log(a), "1" == a.data.status) {
                    var t = a.data.oid;
                    wx.requestPayment({
                        timeStamp: a.data.payInfo.timeStamp,
                        nonceStr: a.data.payInfo.nonceStr,
                        package: a.data.payInfo.package,
                        signType: a.data.payInfo.signType,
                        paySign: a.data.payInfo.paySign,
                        success: function(a) {
                            e.Getmcardinfo(), e.payclose(), setTimeout(function() {
                                e.onLoad();
                            }, 2e3), app.util.request({
                                url: "entry/wxapp/completerechargemcard",
                                data: {
                                    oid: t
                                },
                                method: "POST",
                                cachetime: "0",
                                success: function(a) {
                                    "1" != a.data.status && e.show(a.data.msg);
                                }
                            });
                        },
                        fail: function(a) {
                            e.show("取消支付");
                        },
                        complete: function(a) {
                            console.log(a);
                        }
                    });
                } else e.show(a.data.msg);
            }
        });
    },
    paydisplay: function() {
        this.setData({
            "pay.isHide": !0
        });
    },
    member_discount: function() {
        this.show("当前会员折扣为 " + this.data.level.mcard_level_discount + " 折");
    },
    Consumption: function() {
        this.show("当前消费总额为: " + this.data.mcard.consumption);
    },
    onlinecheck: function() {
        wx.navigateTo({
            url: "/mii_ang/pages/buy/index"
        });
    },
    gopaylog: function() {
        wx.navigateTo({
            url: "/mii_ang/pages/myorder/index?typeid=recharge_mcard"
        });
    },
    GettabBarData: function() {
        var t = this;
        app.util.request({
            url: "entry/wxapp/TabBarData",
            data: {},
            cachetime: "30",
            success: function(a) {
                console.log(a), t.setData({
                    "tabBarData.list": a.data.tabBarData
                });
            }
        });
    }
});