var app = getApp();

Page({
  data: {
    userInfo: {},
    noCard: true,
    tabBarData: {
      thisurl: "mii_ang/pages/personal/index",
      borderStyle: "#d9d9d9",
      // backgroundColor: "#fff",
      list: []
    }
  },
  onLoad: function(options) {
    var a = this;
    wx.setNavigationBarTitle({
      title: "我 的 档 案"
    });

    wx.setNavigationBarColor({
      frontColor: "#ffffff",
      backgroundColor: "#922527"
    });

    //   wx.login({
    //     success: function (res) {
    //       console.log(res)
    //       var code = res.code
    //       wx.getUserInfo({
    //         success: function (res) {
    //           console.log(res);
    //           var userInfo = res.userInfo
    //           var nickName = userInfo.nickName
    //           var avatarUrl = userInfo.avatarUrl
    //           var gender = userInfo.gender //性别 0：未知、1：男、2：女
    //           var province = userInfo.province
    //           var city = userInfo.city
    //           var country = userInfo.country
    //           that.setData({
    //             avatarUrl: userInfo.avatarUrl,
    //             nickName: userInfo.nickName
    //           })
    //           app.util.request({
    //             'url': 'entry/wxapp/openid/zh_hdbm',
    //             'cachetime': '0',
    //             data: { code: code },
    //             success: function (res) {
    //               console.log(res)
    //               var openid = res.data.openid
    //               that.setData({
    //                 openid: res.data.openid
    //               })
    //               wx.setStorageSync("key", res.data.session_key)
    //               wx.setStorageSync("openid", res.data.openid)
    //               wx.setStorageSync("img", avatarUrl)
    //               wx.setStorageSync("name", nickName)
    //               console.log(res.data);
    //               // 报名用户信息
    //               app.util.request({
    //                 'url': 'entry/wxapp/login/zh_hdbm',
    //                 headers: {
    //                   'Content-Type': 'application/json',
    //                 },
    //                 'cachetime': '0',
    //                 data: { openid: openid, img: avatarUrl, name: nickName },
    //                 success: function (res) {
    //                   console.log(res);
    //                   wx.setStorageSync("uid_bm", res.data.id);
    //                   wx.setStorageSync("user_bm", res.data);
    //                   that.setData({
    //                     uid: res.data.id,
    //                     loading: true
    //                   })
    //                 },
    //                 fail: function () {
    //                   console.log("失败了")
    //                 }
    //               });
    //               // 卡券用户信息
    //               app.util.request({
    //                 url: 'entry/wxapp/login',
    //                 headers: {
    //                   'Content-Type': 'application/json',
    //                 },
    //                 'cachetime': '0',
    //                 data: {
    //                   openid: openid
    //                 },
    //                 success: function (res) {
    //                   console.log(res);
    //                   wx.setStorageSync("uid_coupon", res.data[0].uid);
    //                   wx.setStorageSync("user_coupon", res.data);
    //                 },
    //                 fail: function () {
    //                   console.log("获取卡券用户失败");
    //                 }
    //               })
    //               // 前往主页

    //             },
    //             fail: function () {
    //               console.log("获取用户信息失败");
    //               wx: wx.showToast({
    //                 title: '获取信息失败，建议重新启动小程序',
    //                 icon: '',
    //                 image: '',
    //                 duration: 3000,
    //                 mask: true,
    //                 success: function (res) { },
    //                 fail: function (res) { },
    //                 complete: function (res) { },
    //               });
    //               that.setData({buttonDisable: false});
    //             }
    //           })
    //         }
    // a.reload()
    // 获取用户信息
    a.getUserInfo();
    var name = wx.getStorageSync("name"); // 用户昵称
    var avatar = wx.getStorageSync("img"); // 用户头像
    a.setData({ name: name, avatar: avatar });
    console.log(avatar);
  },
  /**
   * 获取会员信息
   */
  getMember: function() {
    var a = this;
    var uid = wx.getStorageSync("uid_coupon");
    console.log(uid);
    app.util.request({
      url: "entry/wxapp/mygetmycardlist",
      data: {
        uid: uid
      },
      cachetime: "10",
      success: function(t) {
        console.log(t);
        for (let card of t.data.cardlist) {
          if (card.card_type == "MEMBER_CARD") {
            console.log("yes member", card);
          }
        }
      }
    });
  },
  /**
   * 获取底部菜单栏
   */
  GetTabBarData: function() {
    var a = this;
    app.util.request({
      url: "entry/wxapp/TabBarData",
      data: {},
      cachetime: "30",
      success: function(t) {
        console.log("yyy", t.data);

        a.setData({
          "tabBarData.list": t.data.tabBarData
        });
      }
    });
  },
  /**
   * 获取卡券信息
   */
  getCardList: function() {
    var a = this;
    var uid = wx.getStorageSync("uid_coupon");
    app.util.request({
      url: "entry/wxapp/mygetmycardlist",
      data: {
        uid: uid
      },
      cachetime: "10",
      //old success
      success: function(t) {
        console.log(t.data);
        if (t.data.cardlist[0]) {
          var current_card_id = t.data.cardlist[0].card_id;
          console.log("Weboost-JL: current_card_id", current_card_id);
          var current_code = t.data.cardlist[0].code;
        }
        //new success
        // success: function (t) {
        //   console.log(t);
        //   for (let card of t.data.cardlist) {
        //     if (card.card_type != "MEMBER_CARD") {
        //       a.setData({
        //         cardList: t.data.cardlist
        //       })
        //     }
        //   }
        // }

        // app.util.request({
        //   url: "entry/wxapp/getmembershipcardinfo",
        //   cachetime: "0",
        //   data:{
        //     card_id: current_card_id,
        //     code: current_code,
        //   },
        //   success: function(temp){
        //   console.log('Weboost-JL: temp', temp.data);

        //   }
        // });
        console.log("Weboost-JL: current_card_id", current_card_id);
        console.log(t.data.cardlist);
        for (let card of t.data.cardlist) {
          if (card.card_type == "MEMBER_CARD") {
            a.setData({
              noCard: false,
              memberCard: card
            });
          }
        }
      }
    });
  },

  /**
   * 获取报名的活动
   */
  getMyActivities: function() {
    var that = this;
    var user_id = that.data.userInfo.memberInfo.uid;
    console.log(user_id);
    app.util.request({
      url: "entry/wxapp/myenroll",
      cachetime: "0",
      data: { user_id: user_id },
      success: function(res) {
        console.log("我报名的活动");
        console.log(res);
        var myenroll = [];
        for (var i = 0; i < res.data.length; i++) {
          if (res.data[i].id != null) {
            var eventList = res.data;
            eventList[i].star_time = eventList[i].start_time.slice(0, 16);
            // res.data[i].address = res.data[i].address.slice(0, 4)
            // res.data[i].end_time = res.data[i].end_time.slice(5, 10)
            // res.data[i].start_time = res.data[i].start_time.slice(5, 10)
            myenroll.push(eventList[i]);
            console.log(myenroll.length);
            var enroll = myenroll[0];
            if (myenroll != null || myenroll != "") {
              var len = myenroll.length;
            } else {
              var len = 0;
            }
            console.log("我报名的活动的长度" + len);
            that.setData({ eventNum: len });
            // 判断报名的状态
            app.util.request({
              url: "entry/wxapp/enroll",
              headers: {
                "Content-Type": "application/json"
              },
              data: { id: enroll.id },
              cachetime: "0",
              success: function(res) {
                console.log(res);
                for (var i = 0; i < res.data.length; i++) {
                  if (res.data[i].user_id == user_id) {
                    that.setData({
                      status: res.data[i].status
                    });
                  }
                }
              }
            });
            that.setData({
              myenroll: myenroll,
              url: url,
              enroll: enroll,
              len: len
            });
          }
        }
        if (res.data == null || res.data == "") {
          that.setData({
            len: 0
          });
        }
        console.log(myenroll);
      }
    });
  },

  /**
   * 领取会员卡
   */
  addMemberCard: function() {
    var that = this;
    // 判断是否已经领取
    var noCard = true;
    app.util.request({
      url: "entry/wxapp/getmycardlist",
      data: {},
      cachetime: "10",
      success: function(t) {
        console.log(t.data);
        for (let card of t.data.cardlist) {
          if (card.card_type == "MEMBER_CARD") {
            noCard = false;
          }
        }

        if (noCard) {
          // 领取会员卡
          app.util.request({
            url: "entry/wxapp/getmcsamembercard",
            data: {},
            catchtime: "10",
            success: function(t) {
              console.log(t);

              var card_id = t.data[0].card_id;
              //       var user_id = that.data.userInfo.memberInfo.uid;
              var uid = wx.getStorageSync("uid_coupon");
              var cid = "";
              //       console.log(n);
              console.log(uid);
              app.util.request({
                url: "entry/wxapp/getvipcard",
                data: {
                  uniacid: 30,
                  cardType: "MEMBER_CARD"
                },
                success: function(t) {
                  cid = t.data[0].id;
                  console.log(cid);
                  app.util.request({
                    url: "entry/wxapp/myaddcard",
                    data: {
                      card_id: card_id,
                      uid: uid,
                      cid: cid,
                      card_type: "MEMBER_CARD",
                      uniacid: 30
                    },
                    cachetime: "30",
                    success: function(t) {
                      wx.addCard({
                        cardList: [
                          {
                            cardId: t.data.cardArray.card_id,
                            cardExt:
                              '{"code": "", "openid":"' +
                              t.data.cardArray.openid +
                              '", "timestamp":"' +
                              t.data.cardArray.timestamp +
                              '","nonce_str":"' +
                              t.data.cardArray.nonceStr +
                              '", "signature":"' +
                              t.data.cardArray.signature +
                              '"}'
                          }
                        ],
                        success: function(t) {
                          console.log(t);
                          var a = t.cardList;
                          app.util.request({
                            url: "entry/wxapp/insCard",
                            data: {
                              card_id: card.card_id,
                              code: a,
                              card_on: card.card_on,
                              card_type: card.card_type,
                              iid: card.id
                            },
                            cachetime: "30",
                            success: function(t) {
                              e.show(t.data.msg);
                            }
                          });
                        },
                        fail: function(t) {
                          console.log("add card failed", t);
                          /*   app.util.request({
                            url: "entry/wxapp/insCard",
                            data: {
                              card_id: card.card_id,
                              code: "",
                              card_on: card.card_on,
                              card_type: card.card_type,
                              iid: card.id
                            },
                            cachetime: "30",
                            success: function(t) {
                              e.show(t.data.msg);
                            },
                            fail: function(t) {
                              console.log("add card to database failed", t);
                            }
                          }); */
                        }
                      });
                    }
                  });
                },
                fail: function(t) {
                  console.log(t);
                }
              });
              console.log(cid);
            }
          });
        }
      }
    });
  },

  /**
   * 获取优惠券
   */
  getMyCoupons: function() {},

  /**
   * 获取底部菜单栏
   */
  getTabBarData: function() {
    var a = this;
    app.util.request({
      url: "entry/wxapp/TabBarData",
      data: {},
      cachetime: "30",
      success: function(t) {
        console.log("tabBar", t.data);
        a.setData({
          "tabBarData.list": t.data.tabBarData
        });
      }
    });
  },
  // onLoad: function (options) {
  //   var a = this;
  //   wx.setNavigationBarTitle({
  //     title: "我 的 档 案"
  //   });
  //   a.GetTabBarData();
  //   a.GetCardList();
  // },
  personal: function(t) {
    var a = this;
    app.util.footer(a);
    wx.login({
      success: function(t) {
        app.util.request({
          url: "entry/wxapp/personal",
          data: {
            code: t.code
          },
          cache: "0",
          success: function(t) {
            console.log(t),
              a.setData({
                "tabBarData.thisurl": a.route,
                booknum: t.data.booknum,
                cardnum: t.data.cardnum,
                uid: t.data.uid,
                copyright: t.data.copyright,
                phone: t.data.phone,
                uname: t.data.uName,
                uphone: t.data.uPhone,
                //  "tabBarData.list": t.data.bottommenu,
                "input.isHide": 1 == t.data.ishide,
                "input.uname": t.data.uName,
                "input.uphone": t.data.uPhone
              });
          }
        }),
          a.GettabBarData();
      }
    });
  },
  /**
   * 获取用户信息
   */
  getUserInfo: function(t) {
    var a = this;
    app.util.getUserInfo(function(t) {
      a.setData({
        userInfo: t
      });
      console.log(a.data.userInfo);
      // 获取卡券信息
      a.getCardList();
      // 加载底部菜单
      a.getTabBarData();
      // 获取报名的活动
      a.getMyActivities();
    });
  }
  // close: function(t) {
  //     this.setData({
  //         "input.isHide": !1
  //     });
  // },
  // bind: function(t) {
  //     this.setData({
  //         "input.isHide": !0
  //     });
  // },
  // uname: function(t) {
  //     this.setData({
  //         "input.uname": t.detail.value
  //     });
  // },
  // uphone: function(t) {
  //   this.setData({
  //     "input.uphone": t.detail.value
  //   });
  // },
  // save: function(t) {
  //     var a = this, n = a.data.uid, e = a.data.input.uname, o = a.data.input.uphone, i = a.data.userInfo.nickName;
  //     return "" == n ? (a.show("请输入姓名"), !1) : "" == e ? (a.show("请输入姓名"), !1) : "" == o ? (a.show("请输入联系电话"),
  //     !1) : /.*/.test(o) ? void app.util.request({
  //         url: "entry/wxapp/memberbind",
  //         data: {
  //             uname: e,
  //             uphone: o,
  //             uid: n,
  //             nickname: i
  //         },
  //         cache: "0",
  //         success: function(t) {
  //             "1" == t.data.status && a.setData({
  //                 "input.isHide": !1
  //             }), a.show(t.data.msg), console.log(a.data);
  //         }
  //     }) : (a.show("联系电话错误"), !1);
  // },
  // show: function(t) {
  //     var a = this;
  //     this.setData({
  //         "toast.isHide": !0,
  //         "toast.content": t
  //     }), setTimeout(function() {
  //         a.setData({
  //             "toast.isHide": !1
  //         });
  //     }, 2e3);
  // },
  // tel: function(t) {
  //     wx.makePhoneCall({
  //         phoneNumber: this.data.phone
  //     });
  // },
  // goindex: function() {
  //     wx.navigateTo({
  //         url: "/mii_ang/pages/index/index"
  //     });
  // },
  // onShareAppMessage: function(t) {
  //     var a = this;
  //     return "button" === t.from && console.log(t.target), {
  //       title: '微思科技WeBoost——全澳首家小程序本地专业开发',
  //       path: "/" + a.data.thispath,
  //       imageUrl: '/images/beautyShare.jpg',
  //         success: function(t) {
  //             a.show("感谢分享");
  //         },
  //         fail: function(t) {
  //             a.show("分享取消");
  //         }
  //     };
  // },
  // GettabBarData: function() {
  //     var a = this;
  //     app.util.request({
  //         url: "entry/wxapp/TabBarData",
  //         data: {},
  //         cachetime: "30",
  //         success: function(t) {
  //              console.log("yyy",t.data);

  //             a.setData({
  //                 "tabBarData.list": t.data.tabBarData
  //             });
  //         }
  //     });
  // }
});
