// mii_ang/pages/personal/myinfo.js
const app = getApp();

Page({

  /**
   * Page initial data
   */
  data: {

  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: "Weboost Pty Ltd"
    });
    wx.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: '#77191d'
    });
  },

  /**
   * 提交表单
   */
  go: function (e) {
    var a = this;
    console.log(a.data.id);
    // 用户信息
    var surname = e.detail.value.surname;  // 姓氏
    var firstname = e.detail.value.firstname; // 名字
    var phone = e.detail.value.phone.trim(); // 电话
    var uid = wx.getStorageSync("uid_coupon");
    console.log(e.detail.value);

    // 表单验证
    var phoneReg = new RegExp(/^\d{10}$/);
    console.log(phone);
    console.log(phoneReg.test(phone));
    // 非空校验
    if (surname.trim().length == 0
      || firstname.trim().length == 0
      || phone.trim().length == 0) {
      wx: wx.showToast({
        title: '请输入完整信息',
        icon: '',
        image: '',
        duration: 3000,
        mask: true,
        success: function (res) { },
        fail: function (res) { },
        complete: function (res) { },
      })
    } else if (!phoneReg.test(phone)) {
      wx: wx.showToast({
        title: '电话格式不正确',
        icon: '',
        image: '',
        duration: 3000,
        mask: true,
        success: function (res) { },
        fail: function (res) { },
        complete: function (res) { },
      })
    } else {
      // 修改个人信息
      app.util.request({
        'url': 'entry/wxapp/updatepersonalinfo',
        'cachetime': '0',
        data: {
          uid: uid,
          name: surname + ' ' + firstname,
          tel: phone,
        },
        success: function (res) {
          console.log('修改成功');
          console.log(res);
          wx.reLaunch({
            url: 'index'
          })
        }
      })
    }
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function () {

  },

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function () {

  },

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function () {

  },

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function () {

  },

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function () {

  },

  /**
   * Called when page reach bottom
   */
  onReachBottom: function () {

  },

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function () {

  }
})