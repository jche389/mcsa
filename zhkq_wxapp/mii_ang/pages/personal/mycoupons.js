// mii_ang/pages/personal/mycoupons.js
const app = getApp();
Page({
  /**
   * Page initial data
   */
  data: {
    url: ""
  },

  /**
   * Lifecycle function--Called when page load
   */
  onLoad: function(options) {
    var that = this;
    wx.setNavigationBarTitle({
      title: "My Coupons"
    });
    wx.setNavigationBarColor({
      frontColor: "#ffffff",
      backgroundColor: "#77191D"
    });
    that.getUrl();
    that.getCardList();
  },

  /**
   * 商家详情
   */
  viewCompany: function(e) {
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    });
  },

  /**
   * 获取卡券信息
   */
  getCardList: function() {
    var a = this;
    var uid = wx.getStorageSync("uid_coupon");
    console.log(uid);
    app.util.request({
      url: "entry/wxapp/mygetmycardlist",
      data: {
        uid: uid
      },
      cachetime: "10",
      success: function(t) {
        console.log(t.data.cardlist);

        //not fixed
        for (let card of t.data.cardlist) {
          if (card.card_type != "MEMBER_CARD") {
            a.setData({
              cardList: t.data.cardlist
            });
          }
        }
      }
    });
  },

  /**
   * 获取服务器路径
   */
  getUrl: function() {
    var that = this;
    app.util.request({
      url: "entry/wxapp/attachurl/zh_hdbm",
      headers: {
        "Content-Type": "application/json"
      },
      cachetime: "0",
      success: function(res) {
        var url = res.data;
        wx.setStorageSync("url", res.data);
        that.setData({
          url: url
        });
        console.log(res);
      }
    });
  },

  /**
   * 查看卡券
   */
  openCard: function(t) {
    console.log(t.currentTarget);
    wx.openCard({
      cardList: [
        {
          cardId: t.currentTarget.dataset.cid,
          code: t.currentTarget.dataset.code.substring(
            1,
            t.currentTarget.dataset.code.length - 1
          )
        }
      ]
    });
  },

  /**
   * Lifecycle function--Called when page is initially rendered
   */
  onReady: function() {},

  /**
   * Lifecycle function--Called when page show
   */
  onShow: function() {},

  /**
   * Lifecycle function--Called when page hide
   */
  onHide: function() {},

  /**
   * Lifecycle function--Called when page unload
   */
  onUnload: function() {},

  /**
   * Page event handler function--Called when user drop down
   */
  onPullDownRefresh: function() {},

  /**
   * Called when page reach bottom
   */
  onReachBottom: function() {},

  /**
   * Called when user click on the top right corner to share
   */
  onShareAppMessage: function() {}
});
