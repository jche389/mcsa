// zh_hdbm/pages/code/code.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    var type = options.type
    console.log(options)
    var bm_id = options.bm_id
    var publish_id = options.publish_id
    that.setData({
      type: options.type,
      money: options.money,
      xm: options.xm,
      sjh: options.sjh,
      name: options.name,
      start_time: options.st_time,
      address: options.address,
      money: options.money,
      ad_id: options.ad_id,
      xingming: options.xingming,
      dianhua: options.dianhua,
      bm_id: bm_id,
      publish_id: publish_id,
    })
    var user_id = wx.getStorageSync('uid_bm')
    app.util.request({
      'url': 'entry/wxapp/getdata/zh_hdbm',
      'cachetime': '0',
      data: { user_id: user_id },
      success: function (res) {
        console.log(res)
        that.setData({
          infos: res.data
        })
      },
    })
    console.log(user_id)
    // 判断报名的状态
    app.util.request({
      'url': 'entry/wxapp/enroll/zh_hdbm',
      headers: {
        'Content-Type': 'application/json',
      },
      data: { id: options.ad_id },
      'cachetime': '0',
      success: function (res) {
        console.log(res)
        for (var i = 0; i < res.data.length; i++) {
          if (res.data[i].user_id = user_id) {
            that.setData({
              status: res.data[i].status
            })
          }
        }
      },
    })

    // 获取随机数
    app.util.request({
      'url': 'entry/wxapp/rand/zh_hdbm',
      'cachetime': '0',
      // data: { user_id: user_id },
      success: function (res) {
        console.log(res)
        that.setData({
          nuber: res.data
        })
      },
    })
    //  获取报名的活动列表
    app.util.request({
      'url': 'entry/wxapp/myenroll/zh_hdbm',
      headers: {
        'Content-Type': 'application/json',
      },
      data: { user_id: user_id },
      'cachetime': '0',
      success: function (res) {
        console.log('这是报名的活动列表')
        console.log(res)
        var publish_id = res.data[0].publish_id
        //获取二维码
        console.log(user_id)
        console.log(res.data[0].bm_id)
        console.log(res.data[0].publish_id)
        app.util.request({
          'url': 'entry/wxapp/bmcode/zh_hdbm',
          'cachetime': '0',
          data: { user_id: user_id, bm_id: res.data[0].bm_id, publish_id: res.data[0].publish_id },
          success: function (res) {
            console.log(res)
            that.setData({
              bath: res.data
            })
          },
        })
        var slider = res.data
        for (var i = 0; i < slider.length; i++) {
          if (options.ad_id == slider[i].id) {
            var bm_id = slider[i].bm_id
            app.util.request({
              'url': 'entry/wxapp/enrolldetails/zh_hdbm',
              headers: {
                'Content-Type': 'application/json',
              },
              data: { bm_id: bm_id, id: options.ad_id },
              'cachetime': '0',
              success: function (res) {

                console.log('获取报名信息')
                console.log(res)
                that.setData({
                  types: res.data.item1.type
                })
                // 判断是平台发布还是个人用户发布
                if (res.data.item1.type == 1) {
                  console.log('平台发布')
                  var slider = res.data.item1
                  slider.end_time = slider.end_time.slice(5, 10)
                  slider.start_time1 = slider.start_time.slice(5, 10)
                  var infos = res.data.item1
                  // 获取平台信息
                  app.util.request({
                    'url': 'entry/wxapp/seller/zh_hdbm',
                    headers: {
                      'Content-Type': 'application/json',
                    },
                    'cachetime': '0',
                    success: function (res) {
                      console.log(res)
                      that.setData({
                        xinxi: res.data,
                        infos: infos,
                      })
                    },
                  })
                } else {
                  console.log('个人用户发布')
                  that.setData({
                    infos: res.data.item2,
                    slider: res.data.item1,
                    xinxi: res.data.item3
                  })
                }
                console.log(infos)
                that.setData({
                  slider: res.data.item1
                })
              },
            })
          }
        }
        that.setData({
          slider: res.data
        })
      },
    })
    //获取报名的活动的详细信息

    // 获取主办方信息
    // if(type==2){
    //   app.util.request({
    //     'url': 'entry/wxapp/seller',
    //     headers: {
    //       'Content-Type': 'application/json',
    //     },
    //     'cachetime': '0',
    //     success: function (res) {
    //       that.setData({
    //         infos: res.data
    //       })
    //     },
    //   })
    // }else{
    //   app.util.request({
    //     'url': 'entry/wxapp/getdata',
    //     'cachetime': '0',
    //     data: { user_id: user_id },
    //     success: function (res) {
    //       console.log(res)
    //       that.setData({
    //         infos: res.data
    //       })
    //     },
    //   })
    // }

  },
  fabu: function (e) {
    wx: wx.reLaunch({
      url: '../activity/activity',
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  fanhui: function (e) {
    wx: wx.reLaunch({
      url: '../index/index',
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  tomap: function (e) {
    var that = this
    // console.log(that.data)
    wx.getLocation({
      type: 'gcj02', //返回可以用于wx.openLocation的经纬度  
      success: function (e) {
        var latitude = e.latitude
        var longitude = e.longitude
        wx.openLocation({
          latitude: latitude,
          longitude: longitude,
          address: that.data.address,
          name: that.data.name,
          scale: 28
        })
      }
    })
  },
  call_phone: function () {
    var that = this
    console.log(that.data)
    wx.makePhoneCall({
      phoneNumber: that.data.xinxi.tel
    })


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})