var app = getApp();

Page({
    data: {
        page: 1,
        book: [],
        openid: "",
        uid: "",
        show: "true",
        toast: {
            isHide: !1,
            content: ""
        },
        orders: {
            isHide: !1,
            info: []
        },
        empty: "false"
    },
    onLoad: function(t) {
        this.Getmybooks();
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onReachBottom: function() {
        var t = this, a = t.data.uid;
        "true" == t.data.show ? t.Getmybook(a) : t.show("没有更多了");
    },
    onPullDownRefresh: function() {
        var t = this;
        t.setData({
            show: "true",
            book: []
        }), t.Getmybook(t.data.uid);
    },
    Getuserbook: function() {
        var a = this;
        wx.login({
            success: function(t) {
                app.util.request({
                    url: "entry/wxapp/user",
                    data: {
                        code: t.code
                    },
                    cache: "30",
                    success: function(t) {
                        a.Getmybook(t.data.openid);
                    }
                });
            }
        });
    },
    Getuserbooks: function() {
        var a = this;
        wx.login({
            success: function(t) {
                app.util.request({
                    url: "entry/wxapp/user",
                    data: {
                        code: t.code
                    },
                    cache: "30",
                    success: function(t) {
                        a.setData({
                            openid: t.data.openid
                        }), a.Getmybooks(t.data.openid);
                    }
                });
            }
        });
    },
    Getmybook: function(t) {
        var a = this;
        app.util.request({
            url: "entry/wxapp/getmybook",
            data: {
                uid: t,
                page: a.data.page
            },
            success: function(t) {
                a.setData({
                    page: a.data.page + 1,
                    book: a.data.book.concat(t.data.book),
                    show: t.data.show
                });
            }
        });
    },
    Getmybooks: function(t) {
        var a = this, o = (t = t, a.data.page);
        app.util.request({
            url: "entry/wxapp/getmybook",
            data: {
                openid: t,
                page: o
            },
            success: function(t) {
                a.setData({
                    page: a.data.page + 1,
                    book: null != t.data.book ? a.data.book.concat(t.data.book) : "",
                    uid: t.data.uid,
                    show: t.data.show,
                    empty: null != t.data.book ? t.data.empty : "true"
                }), setTimeout(function() {
                    wx.setNavigationBarColor({
                        frontColor: "#000000",
                        backgroundColor: "#FFFFFF",
                        animation: {
                            duration: 400,
                            timingFunc: "easeIn"
                        }
                    }), wx.setNavigationBarTitle({
                        title: t.data.title
                    });
                }, 100);
            }
        });
    },
    show: function(t) {
        var a = this;
        this.setData({
            "toast.isHide": !0,
            "toast.content": t
        }), setTimeout(function() {
            a.setData({
                "toast.isHide": !1
            });
        }, 2e3);
    },
    cancel: function(a) {
        var o = this;
        console.log(a), 1 == a.currentTarget.dataset.type ? wx.showModal({
            title: "提示",
            content: "您确定要取消该预约吗？",
            success: function(t) {
                t.confirm ? app.util.request({
                    url: "entry/wxapp/cancelbook",
                    data: {
                        oid: a.currentTarget.dataset.oid
                    },
                    cachetime: "0",
                    success: function(t) {
                        "1" == t.data.status ? (o.show(t.data.msg), o.data.book.splice(a.currentTarget.dataset.index, 1), 
                        o.setData({
                            book: o.data.book
                        })) : o.show(t.data.msg);
                    }
                }) : t.cancel && console.log("用户点击取消");
            }
        }) : o.show("在线支付预约订单,请到门店核对后取消！");
    },
    getorderdetails: function(t) {
        var a = this, o = t.currentTarget.dataset.oid;
        app.util.request({
            url: "entry/wxapp/getorderdetails",
            data: {
                oid: o
            },
            cachetime: "0",
            success: function(t) {
                console.log(t), t.data.erron ? a.show(t.data.msg) : a.setData({
                    "orders.isHide": !0,
                    "orders.info": t.data.data
                });
            }
        });
    },
    close: function() {
        this.setData({
            "orders.isHide": !1,
            "orders.info": []
        });
    }
});