var app = getApp(), t = require("../../template/Toast.js");

Page({
    data: {
        goodsInfo: {},
        goodsprice: "",
        userInfo: {},
        uname: "",
        phone: "",
        skey: [],
        svalue: [],
        service: [],
        serviceId: "",
        date: "",
        time: "",
        sid: "",
        starttime: "",
        endtime: "",
        openid: "",
        toopenid: "",
        address: "",
        tid: "",
        tkey: "",
        tvalue: "",
        tsid: "",
        tArr: [],
        Pageinfo: {
            title: "",
            book_Tname: "",
            book_Taddress: "",
            book_Tservice: "",
            book_Tech: "",
            book_Tdate: "",
            book_Tcard: "",
            book_hideaddress: "",
            book_hidecard: ""
        },
        disabled: "false",
        sessionid: "",
        cardNum: "",
        Gcard: {
            isHide: !1,
            animationData: {},
            cardList: []
        },
        TimeSlot: {
            isHide: !1,
            animationData: {},
            Days: [],
            active: {
                week: "",
                Time: "",
                start: "",
                iSrestDay: ""
            }
        },
        usecardInfo: {
            cid: "",
            oid: "",
            typeid: ""
        },
        seletecardtitle: ""
    },
    onLoad: function(a) {
        var e = this, i = a.gid, s = a.tid;
        e.GetbookPageInfo(), null == i && null == s || (null != i && null == s ? e.GetgoodsInfo(i) : null == i && null != s && e.GetbookInfo(s), 
        e.Getuser(), e.GetuserInfo()), app.util.footer(this), new t.ToastPannel();
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    GetgoodsInfo: function(a) {
        var e = this;
        app.util.request({
            url: "entry/wxapp/getservicelist",
            data: {
                gid: a
            },
            cachetime: "30",
            success: function(t) {
                console.log(t), e.setData({
                    goodsInfo: t.data.goods,
                    goodsprice: t.data.goods.price,
                    skey: t.data.skey,
                    svalue: t.data.svalue,
                    service: t.data.service,
                    serviceId: a,
                    sid: t.data.sid,
                    starttime: t.data.starttime,
                    endtime: t.data.endtime,
                    tkey: t.data.tkey,
                    tvalue: t.data.tvalue,
                    tsid: t.data.tsid,
                    tArr: t.data.tArr,
                    tid: t.data.tsid,
                    cardNum: t.data.cardNum
                });
            }
        }), e.changegoodsCardstatus();
    },
    GetbookInfo: function(t) {
        var a = this;
        app.util.request({
            url: "entry/wxapp/getservicelistintech",
            data: {
                tid: t
            },
            cachetime: "30",
            success: function(t) {
                a.setData({
                    goodsInfo: t.data.goods,
                    goodsprice: t.data.goods.price,
                    skey: t.data.skey,
                    svalue: t.data.svalue,
                    service: t.data.service,
                    serviceId: t.data.gid,
                    sid: t.data.sid,
                    time: t.data.time,
                    starttime: t.data.starttime,
                    endtime: t.data.endtime,
                    date: t.data.date,
                    tkey: t.data.tkey,
                    tvalue: t.data.tvalue,
                    tsid: t.data.tsid,
                    tArr: t.data.tArr,
                    cardNum: t.data.cardNum
                });
            }
        }), a.changegoodsCardstatus();
    },
    bindDateChange: function(t) {
        this.setData({
            date: t.detail.value
        });
    },
    bindTimeChange: function(t) {
        this.setData({
            time: t.detail.value
        });
    },
    bindCountryCodeChange: function(t) {
        var a = this;
        console.log("picker country code 发生选择改变，携带值为", t.detail.value), a.setData({
            serviceId: a.data.skey[t.detail.value]
        }), a.GetgoodsInfo(a.data.skey[t.detail.value]);
    },
    bindCountryCodeChangetech: function(t) {
        this.setData({
            tsid: this.data.tkey[t.detail.value]
        });
    },
    uname: function(t) {
        this.setData({
            uname: t.detail.value
        });
    },
    phone: function(t) {
      this.setData({
        phone: t.detail.value
      });
    },
    address: function(t) {
        this.setData({
            address: t.detail.value
        });
    },
    SaveBook: function(t) {
        var e = this;
        if ("" == e.data.userInfo) return e.show("请允许系统获取您的信息"), e.GetuserInfo(), !1;
        var i = t.detail.formId, a = (t.target.dataset.dis, e.data.uname), s = e.data.phone, o = e.data.serviceId, n = e.data.starttime, d = e.data.endtime, c = e.data.address, r = null == e.data.tsid ? "-1" : e.data.tsid, u = "-1" == r ? "未选择" : e.data.tArr[r], l = e.data.usecardInfo.cid, m = e.data.usecardInfo.oid;
        return "true" == e.data.disabled ? (e.show("请勿重复提交"), !1) : "" == a ? (e.show("请输入姓名"), 
        !1) : "" == s ? (e.show("请输入联系电话"), !1) : c || "-1" == e.data.Pageinfo.book_hideaddress ? /.*/.test(s) ? n && d ? "" == o ? (e.show("请选择服务项目"), 
        !1) : (e.setData({
            disabled: "true"
        }), void app.util.request({
            url: "entry/wxapp/save_book",
            data: {
                uname: a,
                phone: s,
                address: c,
                serviceId: o,
                starttime: n,
                endtime: d,
                formid: i,
                techid: r,
                techname: u,
                card_id: l,
                card_on: m
            },
            cachetime: "0",
            success: function(t) {
                console.log(t);
                var a = t.data.oid;
                if (1 != t.data.is_pay) "1" == t.data.status ? (wx.showModal({
                    title: "提示",
                    content: t.data.msg,
                    success: function(t) {
                        t.confirm ? wx.navigateTo({
                            url: "/mii_ang/pages/mybook/index"
                        }) : t.cancel && e.show("感谢预约");
                    }
                }), e.sendmsg(i, a)) : (e.show(t.data.msg), e.setData({
                    disabled: "false"
                })); else if (-1 != t.data.payInfo.errno) {
                    t.data.payInfo.package;
                    wx.requestPayment({
                        timeStamp: t.data.payInfo.timeStamp,
                        nonceStr: t.data.payInfo.nonceStr,
                        package: t.data.payInfo.package,
                        signType: t.data.payInfo.signType,
                        paySign: t.data.payInfo.paySign,
                        success: function(t) {
                            e.BookPaySuccess(a), e.sendmsg(i, a);
                        },
                        fail: function(t) {
                            e.BookCancelPayOrFail(a), e.setData({
                                disabled: "false"
                            });
                        },
                        complete: function(t) {
                            console.log(t);
                        }
                    });
                } else e.show("参数错误，唤起支付失败"), e.BookCancelPayOrFail(a), e.setData({
                    disabled: "false"
                });
            }
        })) : (e.show("请选择服务时段"), !1) : (e.show("联系电话错误"), !1) : (e.show("请填写联系地址"), !1);
    },
    BookPaySuccess: function(t) {
        var a = this;
        t = t;
        app.util.request({
            url: "entry/wxapp/bookpaysuccess",
            data: {
                oid: t
            },
            cachetime: "0",
            success: function(t) {
                console.log(t), "1" == t.data.status ? wx.showModal({
                    title: "提示",
                    content: t.data.msg,
                    success: function(t) {
                        t.confirm ? wx.navigateTo({
                            url: "/mii_ang/pages/mybook/index"
                        }) : t.cancel && a.show("感谢预约");
                    }
                }) : a.show(t.data.msg);
            }
        });
    },
    BookCancelPayOrFail: function(t) {
        var a = this, e = t;
        app.util.request({
            url: "entry/wxapp/bookcancelpayorfail",
            data: {
                oid: e
            },
            cachetime: "0",
            success: function(t) {
                a.setData({
                    disabled: "false"
                });
            },
            fail: function(t) {
                a.show("操作失败，请联系管理员"), a.setData({
                    disabled: "false"
                });
            }
        });
    },
    Getuser: function() {
        var a = this;
        wx.login({
            success: function(t) {
                app.util.request({
                    url: "entry/wxapp/user",
                    data: {
                        code: t.code
                    },
                    cachetime: "30",
                    success: function(t) {
                        a.setData({
                            openid: t.data.openid,
                            toopenid: t.data.toopenid,
                            uname: t.data.uName,
                            phone: t.data.uPhone
                        });
                    }
                });
            }
        });
    },
    getaddress: function(t) {
        var a = this;
        wx.chooseLocation({
            success: function(t) {
                console.log(t.address), a.setData({
                    address: t.address
                });
            }
        });
    },
    GetuserInfo: function(t) {
        var a = this;
        app.util.getUserInfo(function(t) {
            a.setData({
                sessionid: t.sessionid
            });
        });
    },
    sendmsg: function(t, a) {
        var e = t, i = a;
        app.util.request({
            url: "entry/wxapp/sendMessage",
            data: {
                formid: e,
                oid: i
            },
            cachetime: "0",
            success: function(t) {
                console.log(t);
            }
        });
    },
    getcard: function() {
        var a = this;
        if ("0" == a.data.cardNum) return a.show("抱歉,没有可供您选择使用的优惠券。"), !1;
        app.util.request({
            url: "entry/wxapp/getusecard",
            data: {
                price: a.data.goodsprice
            },
            cachetime: "0",
            success: function(t) {
                console.log(t), "1" == t.data.status ? (a.setData({
                    "Gcard.cardList": t.data.cardList
                }), a.showcard()) : a.show("抱歉没有可使用的卡券。");
            }
        });
    },
    showcard: function() {
        var t = wx.createAnimation({
            duration: 300,
            timingFunction: "ease-in-out"
        });
        (this.animation = t).translateY(-300).step(), this.setData({
            "Gcard.animationData": this.animation.export(),
            "Gcard.isHide": !0
        });
    },
    showTime: function() {
        var t = wx.createAnimation({
            duration: 300,
            timingFunction: "ease-in-out"
        });
        (this.animation = t).translateY(-300).step(), this.setData({
            "TimeSlot.animationData": this.animation.export(),
            "TimeSlot.isHide": !0
        });
    },
    cancelcard: function(t) {
        var a = t, e = this, i = wx.createAnimation({
            duration: 300,
            timingFunction: "ease-out"
        });
        (e.animation = i).translateY(0).step(), e.setData({
            "Gcard.animationData": e.animation.export()
        }), setTimeout(function() {
            e.setData({
                "Gcard.isHide": !1
            });
        }, 300), "true" != a && e.clearusecardInfo();
    },
    clearusecardInfo: function() {
        this.setData({
            "usecardInfo.cid": "",
            "usecardInfo.oid": "",
            "usecardInfo.typeid": "",
            selectcardtitle: "",
            "goodsInfo.price": this.data.goodsprice
        });
    },
    changegoodsCardstatus: function() {
        this.setData({
            "usecardInfo.cid": "",
            "usecardInfo.oid": "",
            "usecardInfo.typeid": "",
            selectcardtitle: ""
        });
    },
    cancelTimeSlot: function() {
        var t = this, a = wx.createAnimation({
            duration: 300,
            timingFunction: "ease-out"
        });
        (t.animation = a).translateY(0).step(), t.setData({
            "TimeSlot.animationData": t.animation.export()
        }), setTimeout(function() {
            t.setData({
                "TimeSlot.isHide": !1
            });
        }, 300);
    },
    GetbookPageInfo: function() {
        var a = this;
        app.util.request({
            url: "entry/wxapp/getbookpageinfo",
            data: {},
            cachetime: "30",
            success: function(t) {
                setTimeout(function() {
                    wx.setNavigationBarColor({
                        frontColor: "#000000",
                        backgroundColor: "#FFFFFF",
                        animation: {
                            duration: 400,
                            timingFunc: "easeIn"
                        }
                    }), wx.setNavigationBarTitle({
                        title: t.data.title
                    });
                }, 100), a.setData({
                    Pageinfo: t.data
                });
            }
        });
    },
    usecard: function(t) {
        var a = this, e = t.currentTarget.dataset.iid, i = t.currentTarget.dataset.oid, s = t.currentTarget.dataset.tid, o = a.data.goodsprice, n = t.currentTarget.dataset.title;
        app.util.request({
            url: "entry/wxapp/usecard",
            data: {
                cid: e,
                price: o
            },
            cachetime: "0",
            success: function(t) {
                if ("1" == t.data.status) {
                    a.setData({
                        "goodsInfo.price": t.data.price,
                        "usecardInfo.cid": e,
                        "usecardInfo.oid": i,
                        "usecardInfo.typeid": s,
                        selectcardtitle: n
                    });
                    a.cancelcard("true");
                } else a.show(t.data.msg);
            }
        });
    },
    
    Getdate: function() {
        var a = this, t = a.data.tsid;
        app.util.request({
            url: "entry/wxapp/getdate",
            data: {
                tid: t || "0"
            },
            method: "POST",
            cachetime: "0",
            success: function(t) {
                console.log(t), 1 == t.data.status ? (a.setData({
                    "TimeSlot.Days": t.data.date,
                    "TimeSlot.active.week": t.data.date[0].week,
                    "TimeSlot.active.Time": t.data.date[0].TimeSlot,
                    "TimeSlot.active.date": t.data.date[0].date,
                    "TimeSlot.active.iSrestDay": t.data.date[0].iSrestDay
                }), a.showTime()) : a.show(t.data.msg);
            }
        });
    },
    SelectedTime: function(t) {
        var a = t.currentTarget.dataset.start, e = t.currentTarget.dataset.start_cn, i = t.currentTarget.dataset.end, s = t.currentTarget.dataset.end_cn, o = t.currentTarget.dataset.status;
        if (-1 != o) return this.show(0 == o ? "该时间段预约已满,请更换" + this.data.Pageinfo.book_Ttech : "时间段已无效"), 
        !1;
        this.setData({
            "TimeSlot.active.start": a,
            "TimeSlot.active.start_cn": e,
            "TimeSlot.active.end": i,
            "TimeSlot.active.end_cn": s
        });
    },
    changeFilter: function(t) {
        var a = t.currentTarget.dataset.index, e = t.currentTarget.dataset.date, i = t.currentTarget.dataset.rest;
        this.setData({
            "TimeSlot.active": {
                week: t.target.id,
                Time: this.data.TimeSlot.Days[a].TimeSlot,
                date: e,
                iSrestDay: i
            }
        });
    },
    Timeconfirm: function() {
        var t = this;
        if ("" == t.data.TimeSlot.active.date || !t.data.TimeSlot.active.start_cn || !t.data.TimeSlot.active.end_cn) return t.show("请选择预约时间段"), 
        "";
        t.setData({
            date: t.data.TimeSlot.active.date,
            time: t.data.TimeSlot.active.start_cn + "-" + t.data.TimeSlot.active.end_cn,
            starttime: t.data.TimeSlot.active.start,
            endtime: t.data.TimeSlot.active.end
        }), t.cancelTimeSlot();
    }
});