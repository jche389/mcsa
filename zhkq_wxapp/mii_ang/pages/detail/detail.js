var app = getApp(), WxParse = require("../../../wxParse/wxParse.js");

Page({
    data: {
        goodsInfo: {},
        attachurl: "",
        screenWidth: 0,
        screenHeight: 0,
        imgwidth: 0,
        imgheight: 0,
        thispath: "",
        goodsid: ""
    },
    onLoad: function(t) {
        var a = this, i = t.gid;
        a.setData({
            thispath: a.route,
            goodsid: t.gid
        }), null != i && app.util.request({
            url: "entry/wxapp/getgoodsinfo",
            data: {
                uniacid: app.siteInfo.uniacid,
                gid: i
            },
            cachetime: "30",
            success: function(t) {
                a.setData({
                    goodsInfo: t.data,
                    attachurl: t.data.attachurl
                }), WxParse.wxParse("article", "html", t.data.details, a, 5), setTimeout(function() {
                    wx.setNavigationBarColor({
                        frontColor: "#000000",
                        backgroundColor: "#FFFFFF",
                        animation: {
                            duration: 400,
                            timingFunc: "easeIn"
                        }
                    }), wx.setNavigationBarTitle({
                        title: t.data.goodsname
                    });
                }, 100);
            }
        }), console.log(this);
    },
    onReady: function() {
        WxParse.wxParse("article", "html", this.data.goodsInfo.details, this, 5);
    },
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onShareAppMessage: function(t) {
        var a = this, i = a.data.goodsInfo.goodsname;
        return "button" === t.from && console.log(t.target), {
            title: i,
            path: "/" + a.data.thispath + "?gid=" + a.data.goodsid,
            success: function(t) {
                a.show("感谢分享");
            },
            fail: function(t) {
                a.show("分享取消");
            }
        };
    },
    autoImage: function(t) {
        var a = 710 / (t.detail.width / t.detail.height);
        this.setData({
            imgwidth: 710,
            imgheight: a
        });
    },
    show: function(t) {
        var a = this;
        this.setData({
            "toast.isHide": !0,
            "toast.content": t
        }), setTimeout(function() {
            a.setData({
                "toast.isHide": !1
            });
        }, 2e3);
    },
    swiperChange: function(t) {
        this.setData({
            swiperCurrent: t.detail.current
        });
    },
    goto: function(t) {
        wx.navigateTo({
            url: t.currentTarget.dataset.url
        });
    }
});