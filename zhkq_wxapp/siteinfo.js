module.exports = {
    name: "MCSA",
    uniacid: "30",
    acid: "30",
    multiid: "29",
    version: "1.01",
    siteroot: "https://mcsa.weboostapp.com/app/index.php",
    design_method: "3"
};